#include "SceneBase.h"
using namespace CocosDenshion;
using namespace Utility;
USING_NS_CC;

bool SceneBase::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}
	this->scheduleUpdate();

	worldCamera = Camera::create();
	//worldCamera = getDefaultCamera();
	worldCamera->setCameraFlag(CameraFlag::USER1);
	addChild(worldCamera);

	isGrounded = false;

	audioSource = SimpleAudioEngine::getInstance();
	audioSource->preloadBackgroundMusic("Audio/HenesysHuntingGround.mp3");
	audioSource->preloadEffect("Audio/Gun.wav");
	audioSource->preloadEffect("Audio/Arrow.wav");
	audioSource->preloadEffect("Audio/Weapon.wav");

	getDefaultCamera()->setDepth(1);

	SpriteFrameCache* cache = SpriteFrameCache::getInstance();
	AnimationCache* animCache = AnimationCache::getInstance();

	//Sprite Sheet
	SpriteBatchNode* spriteBatch = SpriteBatchNode::create("sprite.png");
	cache->addSpriteFramesWithFile("sprite.plist");
	animCache->addAnimationsWithFile("sprite_ani.plist");

	//making mushmummy test
	SpriteBatchNode* mushBatch = SpriteBatchNode::create("mushmummySprite.png");
	cache->addSpriteFramesWithFile("mushmummy.plist");
	animCache->addAnimationsWithFile("mushmummy_ani.plist");

	//making gangster test
	SpriteBatchNode* gangBatch = SpriteBatchNode::create("leaderSprite.png");
	cache->addSpriteFramesWithFile("leader.plist");
	animCache->addAnimationsWithFile("leader_ani.plist");

	SpriteBatchNode* portalBatch = SpriteBatchNode::create("portal.png");
	cache->addSpriteFramesWithFile("portal.plist");
	animCache->addAnimationsWithFile("portal_ani.plist");

	//making flash Jump
	SpriteBatchNode* jumpBatch = SpriteBatchNode::create("flashJumpSprite.png");
	cache->addSpriteFramesWithFile("flashJump.plist");
	animCache->addAnimationsWithFile("flashJump_ani.plist");

	//making pianus
	SpriteBatchNode* pianus = SpriteBatchNode::create("pianusSprite.png");
	cache->addSpriteFramesWithFile("pianus.plist");
	animCache->addAnimationsWithFile("pianus_ani.plist");

	SpriteBatchNode* ropBatch = SpriteBatchNode::create("rope.png");
	cache->addSpriteFramesWithFile("rope.plist");


}

//initializes enemies, portals, and ropes, and anything else that might be in the object layer 
bool SceneBase::InitializeObjects()
{
	//Create the spawner
	if (!enemySpawner)
		enemySpawner = new EnemySpawner(this);

	//Get the tilemap
	auto tilemap = this->getChildByName("Tilemap");

	//getting a reference to the objects layer in tilemap
	TMXObjectGroup* objectGroup = (dynamic_cast<TMXTiledMap*>(tilemap))->objectGroupNamed("Objects");
	ValueMap spawnPoint;
	std::vector<ValueMap> spawnPoints;
	for (int i = 0; i < objectGroup->getObjects().size(); i++)
	{
		std::string enemyName = "Enemy" + std::to_string(i + 1);

		spawnPoint = objectGroup->getObject(enemyName);
		if (spawnPoint.cbegin() == spawnPoint.cend())//basically stating if the value map obtained has any values attached to it, since i cant just == null/nullptr
			continue;

		spawnPoints.push_back(objectGroup->getObject(enemyName));
	}

	//For now use num of spawn points as max number of units
	//Save the spawn points and the parent-child path to the node to place the spawned sprites in
	enemySpawner->Init("this/enemyNode/", respawnTimerReset, spawnPoints.size() * 2, spawnPoints, this->getChildByName("Tilemap"));
	//std::string testString;
	//for (auto test : spawnPoints)
	//{
	//	testString.append(test.at("x").asString());
	//}
	//log(testString.c_str());

	//for portal, should exist in the same for loop above, but very messy
	for (int i = 0; i < objectGroup->getObjects().size(); i++)
	{
		std::string portalName = "Portal" + std::to_string(i + 1);

		spawnPoint = objectGroup->getObject(portalName);
		if (spawnPoint.cbegin() == spawnPoint.cend())//basically stating if the value map obtained has no values attached to it, since i cant just == null/nullptr
			continue;

		//creating the sprite
		auto portalSprite = Sprite::create();
		portalSprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("portal1.png"));
		portalSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		portalSprite->setName("portal");

		//setting position
		int xPos = (spawnPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
		int yPos = (spawnPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
		portalSprite->setPosition(xPos, yPos);
		portalSprite->setScale(2);

		//adding animations
		Animation* anim = AnimationCache::getInstance()->getAnimation("portal_anim");
		auto portalAnim = RepeatForever::create(Animate::create(anim));
		portalAnim->setTag(ANIMATION);
		portalSprite->runAction(portalAnim);

		//adding physics body
		auto portalBody = PhysicsBody::createBox(Size(portalSprite->getContentSize().width * 0.6f, portalSprite->getContentSize().height), PhysicsMaterial(0.1f, 0.5f, 0.0f));

		//portalBody->setCollisionBitmask(LayerGround);
		portalBody->setCategoryBitmask(LayerTrigger);
		portalBody->setContactTestBitmask(LayerPlayer);
		portalBody->setRotationEnable(false);
		portalBody->setDynamic(false);
		portalSprite->addComponent(portalBody);

		this->getChildByName("spriteNode")->addChild(portalSprite, 1);
	}

	//for rope 
	for (int i = 0; i < objectGroup->getObjects().size(); i++)
	{
		std::string ropeName = "Rope" + std::to_string(i + 1);

		spawnPoint = objectGroup->getObject(ropeName);
		if (spawnPoint.cbegin() == spawnPoint.cend())//basically stating if the value map obtained has no values attached to it, since i cant just == null/nullptr
			continue;

		float mapScale = tilemap->getScale();
		float heightDiff = spawnPoint.at("height").asFloat() * mapScale;
		//creating the top rope sprite
		auto ropeSprite = Sprite::create();
		ropeSprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("rope_top.png"));
		ropeSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
		ropeSprite->setName("rope");

		//setting position
		float xPos = (spawnPoint.at("x").asFloat() + tilemap->getPositionX()) * mapScale;
		float yPosEnd = (spawnPoint.at("y").asFloat() + tilemap->getPositionY()) * mapScale;
		float yPosStart = yPosEnd + heightDiff;
		ropeSprite->setPosition(xPos, yPosStart);
		ropeSprite->setScale(mapScale);

		this->getChildByName("spriteNode")->addChild(ropeSprite, 1);

		//creating the middle of the rope sprite
		//theres 3 values are trying to calculate "stretching" the rope, so when theres like a .4 (or whatever decimal value), can stretch all sections of the rope a bit
		float midSecRange= (spawnPoint.at("height").asFloat()) / SpriteFrameCache::getInstance()->getSpriteFrameByName("rope_bot.png")->getOriginalSize().height;
		int midSecCount = ceil(midSecRange);
		float scaleFactor = midSecRange / (float)midSecCount;
		for (int j = 1; j < midSecCount; j++)  //j starts at 1 as 0 is at the base/ end of the rope
		{

			//creating the bottom rope sprite
			ropeSprite = Sprite::create();
			ropeSprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("rope_mid.png"));
			ropeSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
			ropeSprite->setName("rope");

			ropeSprite->setPosition(xPos, (yPosEnd + ((float)j * scaleFactor / (float)midSecRange * heightDiff))); //math part is getting the position of rope for that "part"
			ropeSprite->setScaleX(mapScale);
			ropeSprite->setScaleY(mapScale * scaleFactor);
			
			int n = ceil((float)midSecCount *0.5f); //didnt test for if midSecCount is odd yet
			if (j == n)
			{
				//adding physics body
				auto ropeBody = PhysicsBody::createBox(Size(spawnPoint.at("width").asFloat(), spawnPoint.at("height").asFloat()), PhysicsMaterial(0.1f, 0.5f, 0.0f));

				//portalBody->setCollisionBitmask(LayerGround);
				ropeBody->setCategoryBitmask(LayerTrigger);
				ropeBody->setContactTestBitmask(LayerPlayer);
				ropeBody->setRotationEnable(false);
				ropeBody->setDynamic(false);
				ropeSprite->addComponent(ropeBody);

			}

			this->getChildByName("spriteNode")->addChild(ropeSprite, 1);

		}

		//creating the bottom rope sprite
		ropeSprite = Sprite::create();
		ropeSprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("rope_bot.png"));
		ropeSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
		ropeSprite->setName("rope");

		ropeSprite->setPosition(xPos, yPosEnd);
		ropeSprite->setScale(mapScale);

		////adding animations
		//Animation* anim = AnimationCache::getInstance()->getAnimation("portal_anim");
		//auto portalAnim = RepeatForever::create(Animate::create(anim));
		//portalAnim->setTag(ANIMATION);
		//ropeSprite->runAction(portalAnim);


		this->getChildByName("spriteNode")->addChild(ropeSprite, 1);

	}

	return true;
}

void SceneBase::update(float dt)
{
	//Update enemy spawner
	if (enemySpawner)
		enemySpawner->Update(dt);
}

TMXTiledMap* SceneBase::InitializeTilemap(std::string filePath, std::string name, Vec2 position, float scale)
{
	TMXTiledMap* tilemap = TMXTiledMap::create(filePath);

	tilemap->setName(name);
	tilemap->setPosition(position);
	tilemap->setScale(scale);

	int mapWidth = tilemap->getMapSize().width;
	int mapHeight = tilemap->getMapSize().height;
	//3 is hardcoded because Tiled lawl
	for (int i = 0; i < 3; i++)
	{
		std::string layerName = "TileLayer" + std::to_string(i + 1);
		auto spriteLayer = tilemap->getLayer(layerName);
		if (spriteLayer == nullptr)
		{
			continue; //or break;
		}

		//combining hitboxes
		std::vector<Vec2> listOfPoints;
		Size bounds = spriteLayer->getLayerSize();
		Vec2 previousPoint, currentPoint;
		Vec2 directions[] = { Vec2(1,-1),Vec2(1,0) ,Vec2(1,1) ,Vec2(0,1) ,Vec2(-1,1) ,Vec2(-1,0) ,Vec2(-1,-1) ,Vec2(0,-1) };

		bool findingNeighbours = false;
		int currentDirection = 0;	//Current Direction to check
		int backDirection = -1;		//Opposite direction of previous direction

		//stores completed ground physics bodies
		std::vector<std::vector<Vec2>> groundPhysicsBodies;

		//Find the most top pixel on the left
		for (int x = 0; x < mapWidth && !findingNeighbours; x++)
		{
			//std::string tileRow;
			for (int y = 0; y < mapHeight; y++)
			{
				auto spriteTile = spriteLayer->getTileAt(Vec2(x, y));
				if (spriteTile != NULL)
				{
					Size spriteSize;
					spriteSize = spriteTile->getContentSize();

					/*
					//Rect query
					auto func = [&inRect](PhysicsWorld& world, PhysicsShape& shape, void* userData)->bool
					{
						//If is inside a ground object, skip this point
						if (shape.getCategoryBitmask() == LayerGround)
						{
							inRect = true;
							return false;
						}

						//Return true from the callback to continue rect queries
						return true;
					};

					//Checks against all existing physicsboundaries if its inside any
					Vec2 positionOfTile = Vec2(x * spriteSize.width, y * spriteSize.height) + tilemap->getPosition() * tilemap->getScale();
					((Scene*)(this->getParent()))->getPhysicsWorld()->queryRect(func, Rect(positionOfTile.x, positionOfTile.y, spriteSize.width, spriteSize.height), nullptr);

						*/

					bool inRect = false;
					for (auto groundBody : groundPhysicsBodies)
					{
						if (x == 1 && y == 188)
						{
							log("help");
						}
						if (pointInPolygon(Vec2(x, y), groundBody, groundBody.size()))
						{
							inRect = true;
							break;
						}
					}
					if (inRect)
						continue;
					currentPoint.setPoint(x, y);
					findingNeighbours = true;

					//Stores the points
					std::vector<Vec2> listOfPoints;
					//Finds the neighbours of the points
					while (findingNeighbours)
					{
						//Main Loop
						if (currentPoint.x == 1 && currentPoint.y == 185)
						{
							log("help");
						}
						//Add current point into list
						//Check for straight line b4 push
						//If x||y is same as both previous point AND point before previous point, remove previous point and push current point
						if (listOfPoints.size() >= 2)
						{
							//If at least 2 points already inside

							if ((currentPoint - listOfPoints.back()).getNormalized() == (listOfPoints.back() - listOfPoints.at(listOfPoints.size() - 2)).getNormalized())
							{
								listOfPoints.pop_back();
								//	listOfPoints.pop_back();
							}
						}

						listOfPoints.push_back(currentPoint);

						//Check currentDir
						while (currentDirection != backDirection)
						{
							//Get next point
							Vec2 nextCheck = currentPoint + directions[currentDirection];
							//Reached Endpoint
							if (nextCheck == listOfPoints.front())
							{
								findingNeighbours = false;
								break;
							}
							//Check next direction
							if (nextCheck.x < bounds.width && nextCheck.y < bounds.height && nextCheck.x >= 0 && nextCheck.y >= 0)
							{
								int gid = spriteLayer->getTileGIDAt(nextCheck);
								//0 = dont have, 1 = have
								if (gid)
								{
									//Found Point
									previousPoint = currentPoint;
									currentPoint = nextCheck;
									backDirection = wrap(currentDirection - 4, 0, 8);
									currentDirection = wrap(currentDirection - 3, 0, 8);
									break;
								}
								else
								{
									currentDirection = wrap(currentDirection + 1, 0, 8);
									continue;
								}
							}
							else
							{
								currentDirection = wrap(currentDirection + 1, 0, 8);
								continue;
							}
						}
						if (currentDirection == backDirection)
							break;
					}

					float widthOffset = tilemap->getContentSize().width * 0.5f / spriteSize.width;
					float heightOffset = tilemap->getContentSize().height * 0.5f / spriteSize.height;
					std::reverse(listOfPoints.begin(), listOfPoints.end());

					groundPhysicsBodies.push_back(listOfPoints);

					Vec2* collisionOutline = new Vec2[listOfPoints.size()];
					//std::reverse(listOfPoints.begin(), listOfPoints.end());
					std::copy(listOfPoints.begin(), listOfPoints.end(), collisionOutline);
					for (int i = 0; i < listOfPoints.size(); i++)
					{
						collisionOutline[i].x -= widthOffset;//translating back to origin
						collisionOutline[i].x *= spriteSize.width; //then scale
						collisionOutline[i].x += spriteSize.width * 0.5f; //translate back to point
						//collisionOutline[i].x -= (visibleSize.width * 0.25f);
						collisionOutline[i].y -= heightOffset;
						collisionOutline[i].y *= spriteSize.height;
						collisionOutline[i].y += spriteSize.height * 0.5f;
						collisionOutline[i].y = /*playingSize.height */-collisionOutline[i].y;
					}
					//auto aPhysicsBodyNode = Node::create();

					PhysicsBody* tilePhysics = PhysicsBody::createEdgePolygon(collisionOutline, listOfPoints.size(), PhysicsMaterial(1000000.1f, 0.f, 1.0f));
					tilePhysics->setDynamic(false);
					tilePhysics->setCollisionBitmask(LayerPlayer | LayerEnemy);
					tilePhysics->setCategoryBitmask(LayerGround);
					tilePhysics->setContactTestBitmask(LayerPlayer);
					tilePhysics->setMass(PHYSICS_INFINITY);

					spriteLayer->setPhysicsBody(tilePhysics);
					//tilemap->addChild(aPhysicsBodyNode, 1);
					goto helps;
				}
			}
		}

	helps:
		int x = 0;
	}
	return tilemap;
}

void SceneBase::SpawnEnemies()
{
	auto tilemap = this->getChildByName("Tilemap");

	//getting a reference to the objects layer in tilemap
	TMXObjectGroup* objectGroup = (dynamic_cast<TMXTiledMap*>(tilemap))->objectGroupNamed("Objects");
	ValueMap spawnPoint;

	//	Node* node = this->getChildByName("spriteNode");
	for (int i = 0; i < objectGroup->getObjects().size(); i++)
	{
		std::string enemyName = "Enemy" + std::to_string(i + 1);
		spawnPoint = objectGroup->getObject(enemyName);

		if (spawnPoint.cbegin() == spawnPoint.cend())//basically stating if the value map obtained has any values attached to it, since i cant just == null/nullptr
			continue;

		if (this->getChildByName(enemyName) != nullptr)
			continue;


		auto enemySprite = Sprite::create();
		enemySprite->setName(enemyName);
		//enemySprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("gangster_idle1.png"));

		int xPos = (spawnPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
		int yPos = (spawnPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
		enemySprite->setPosition(xPos, yPos);

		Animation* anim;
		switch (rand() % 3)
		{
		case 0:
		default:
			enemySprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("gangster_die1.png"));
			anim = AnimationCache::getInstance()->getAnimation("gangster_die");
			break;
		case 1:
			enemySprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("gangster_idle1.png"));
			anim = AnimationCache::getInstance()->getAnimation("gangster_idle");
			break;
		case 2:
			enemySprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("gangster_attack1.png"));
			anim = AnimationCache::getInstance()->getAnimation("gangster_attack");
			break;
		}
		enemySprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		//anim = AnimationCache::getInstance()->getAnimation("gangster_die");
		//anim = AnimationCache::getInstance()->getAnimation("gangster_idle");
		//anim = AnimationCache::getInstance()->getAnimation("gangster_attack");
		enemySprite->setScaleX(-abs(enemySprite->getScaleX()));
		auto moveAnim = RepeatForever::create(Animate::create(anim));
		moveAnim->setTag(ANIMATION);
		enemySprite->runAction(moveAnim);

		//adding physics body
		PhysicsBody* ephysicsBody = PhysicsBody::createBox(Size(enemySprite->getContentSize().width, enemySprite->getContentSize().height), PhysicsMaterial(.1f, 0.f, 0.f));
		ephysicsBody->setCollisionBitmask(LayerGround);
		ephysicsBody->setCategoryBitmask(LayerEnemy);
		ephysicsBody->setContactTestBitmask(LayerGround | LayerPlayer);
		ephysicsBody->setRotationEnable(false);
		enemySprite->addComponent(ephysicsBody);
		enemySprite->setCameraMask((unsigned short)CameraFlag::USER1);

		//Add as child of scene
		this->getChildByName("spriteNode")->addChild(enemySprite);
	}
}
int isLeft(Vec2 P0, Vec2 P1, Vec2 P2)
{
	return ((P1.x - P0.x) * (P2.y - P0.y)
		- (P2.x - P0.x) * (P1.y - P0.y));
}
bool SceneBase::pointInPolygon(cocos2d::Vec2 point, std::vector<cocos2d::Vec2> polygon, int n)
{
	int wn = 0;

	for (int i = 0; i < n; i++)
	{
		Vec2 point1 = polygon[i];
		if (point1 == point)
			return true;
		Vec2 point2;
		if (i < n - 1)
		{
			point2 = polygon[i + 1];
		}
		else
		{
			point2 = polygon[0];
		}
		//Test some shit
		{
			int l = isLeft(point1, point2, point);
			if (l > 0)
				++wn;
			else if (l < 0)
				--wn;
			else
				return true;
		}

		/*
		// edge from V[i] to  V[i+1]
		if (point1.y <= point.y)
		{
			// start y <= P.y
			if (point2.y > point.y)      // an upward crossing
			{
				int l = isLeft(point1, point2, point);
				if(l > 0)	//Point left of edge
					++wn;								//Have  a valid up intersect
				if (l == 0)
					return true;
			}
		}
		else
		{                        // start y > P.y (no test needed)
			if (point2.y <= point.y)     // a downward crossing
			{
				int l = isLeft(point1, point2, point);
				if(l < 0)	//Point right of edge
					--wn;								//Have  a valid up intersect
				if (l == 0)
					return true;
			}
		}
		*/
	}

	return wn != 0;
}

//Cleanup stuff
SceneBase::~SceneBase()
{
	if (enemySpawner)
	{
		delete enemySpawner;
		enemySpawner = NULL;
	}
}
