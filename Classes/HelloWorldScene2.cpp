/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "EmptyScene.h"
#include "HelloWorldScene2.h"
#include "Utility.h"
using namespace Utility;

USING_NS_CC;
//kermit suicide
Scene* HelloWorld2::createScene()
{
	//	auto scene = Scene::create();
	auto scene = Scene::createWithPhysics();
	scene->getPhysicsWorld()->setGravity(Vec2(0, -980.f));
	auto layer = HelloWorld2::create();
	scene->addChild(layer, 0, 999);
	scene->getPhysicsWorld()->setDebugDrawMask(0xfffff);
	//return layer;
	return scene;


	//no physics
	//auto layer = HelloWorld2::create();
	//return layer;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	//printf("Error while loading: %s\n", filename);
	//printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorld2Scene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld2::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	this->scheduleUpdate();

	worldCamera = Camera::create();
	worldCamera->setCameraFlag(CameraFlag::USER1);
	addChild(worldCamera);
	isGrounded = false;
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	//this->setPhysics3DDebugCamera(worldCamera);
	auto audioSource = CocosDenshion::SimpleAudioEngine::getInstance();
	if (!audioSource->isBackgroundMusicPlaying())
	{
		audioSource->preloadBackgroundMusic("Audio/HenesysHuntingGround.mp3");
		audioSource->playBackgroundMusic("Audio/HenesysHuntingGround.mp3");
	}
	getDefaultCamera()->setDepth(1);
	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.
	/*
	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create(
										   "CloseNormal.png",
										   "CloseSelected.png",
										   CC_CALLBACK_1(HelloWorld2::menuCloseCallback, this));

	if (closeItem == nullptr ||
		closeItem->getContentSize().width <= 0 ||
		closeItem->getContentSize().height <= 0)
	{
		problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
	}
	else
	{
		float x = origin.x + visibleSize.width - closeItem->getContentSize().width/2;
		float y = origin.y + closeItem->getContentSize().height/2;
		closeItem->setPosition(Vec2(x,y));
	}

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);
	*/
	/////////////////////////////
	// 3. add your codes below...

	// add a label shows "Hello World"
	// create and initialize a label
	/*
	auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
	if (label == nullptr)
	{
		problemLoading("'fonts/Marker Felt.ttf'");
	}
	else
	{
		// position the label on the center of the screen
		label->setPosition(Vec2(origin.x + visibleSize.width/2,
								origin.y + visibleSize.height - label->getContentSize().height));

		// add the label as a child to this layer
		this->addChild(label, 1);
	}

	// add "HelloWorld2" splash screen"
	auto sprite = Sprite::create("HelloWorld2.png");
	if (sprite == nullptr)
	{
		problemLoading("'HelloWorld2.png'");
	}
	else
	{
		// position the sprite on the center of the screen
		sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

		// add the sprite as a child to this layer
		this->addChild(sprite, 0);
	}
	*/


	Size playingSize = Size(visibleSize.width, visibleSize.height - (visibleSize.height / 8));

	//Node Container
	auto nodeItems = Node::create();
	nodeItems->setName("nodeItems");

	//init tile map
	//tilemap = new CCTMXTiledMap();
	//tilemap = TMXTiledMap::create("Tilemaps/Testmap.tmx");
	auto tilemap = (TMXTiledMap*)(this->getChildByName("Tilemap"));
	tilemap = TMXTiledMap::create("Tilemaps/HuntingGround2.tmx");
	tilemap->setName("Tilemap");
	tilemap->setPosition(0, playingSize.height * 0.05f);
	tilemap->setScale(2);

	int mapWidth = tilemap->getMapSize().width;
	int mapHeight = tilemap->getMapSize().height;
	//3 is hardcoded because Tiled lawl
	for (int i = 0; i < 3; i++)
	{
		std::string layerName = "TileLayer" + std::to_string(i + 1);
		auto spriteLayer = tilemap->getLayer(layerName);

		for (int x = 0; x < mapWidth; x++)
		{
			std::string tileRow;
			for (int y = 0; y < mapHeight; y++)
			{
				//to go back to individual square collision
				
				auto spriteTile = spriteLayer->getTileAt(Vec2(x, y));
				if (spriteTile != NULL)
				{
					PhysicsBody* tilePhysics = PhysicsBody::createBox(Size(15.0f, 15.0f), PhysicsMaterial(0.1f, 0.5f, 0.0f));
					//Vec2 points[3] = { Vec2(7.5f,7.5f), Vec2(7.5f,-7.5f), Vec2(-7.5f,-7.5f) };
					//PhysicsBody* tilePhysics = PhysicsBody::createPolygon(points, 3, PhysicsMaterial(1000000.1f, 0.f, 1.0f));

					tilePhysics->setDynamic(false);
					tilePhysics->setCollisionBitmask(LayerPlayer | LayerEnemy);
					tilePhysics->setCategoryBitmask(LayerGround);
					tilePhysics->setContactTestBitmask(LayerPlayer);
					spriteTile->setPhysicsBody(tilePhysics);
					//tileRow.append("1");
				}
				//else
				//{
				//	//tileRow.append("0");
				//}
				
				//auto have = spriteLayer->getTileGIDAt(Vec2(x, y));
				//tileRow.append("%i", have == 0);
			}
			//log(tileRow.c_str());
		}

		//for (int x = 0; x < 100; x++)
		//{
		//	for (int y = 0; y < 200; y++)
		//	{
		//		auto spriteTile = spriteLayer->getTileAt(Vec2(x, y));
		//		
		//		if (spriteTile != NULL)
		//		{
		//			PhysicsBody* tilePhysics = PhysicsBody::createBox(Size(15.0f, 15.0f), PhysicsMaterial(0.1f, 0.5f, 0.0f));
		//			//Vec2 points[3] = { Vec2(7.5f,7.5f), Vec2(7.5f,-7.5f), Vec2(-7.5f,-7.5f) };
		//			//PhysicsBody* tilePhysics = PhysicsBody::createPolygon(points, 3, PhysicsMaterial(1000000.1f, 0.f, 1.0f));

		//			tilePhysics->setDynamic(false);
		//			tilePhysics->setCollisionBitmask(LayerPlayer | LayerEnemy);
		//			tilePhysics->setCategoryBitmask(LayerGround);
		//			tilePhysics->setContactTestBitmask(LayerPlayer);
		//			spriteTile->setPhysicsBody(tilePhysics);
		//		}
		//		
		//	}
		//}

		std::vector<Vec2> listOfPoints;
		Vec2 previousPoint;
		Vec2 currentPoint;
		bool foundStart = false;
		Size bounds = spriteLayer->getLayerSize();
		Vec2 spriteSize;
		Vec2 directions[] = { Vec2(1,-1),Vec2(1,0) ,Vec2(1,1) ,Vec2(0,1) ,Vec2(-1,1) ,Vec2(-1,0) ,Vec2(-1,-1) ,Vec2(0,-1) };
		int currentDirection = 0;	//Current Direction to check
		int backDirection = -1;		//Opposite direction of previous direction

		//Find the most top pixel on the left
		for (int x = 0; x < mapWidth && !foundStart; x++)
		{
			std::string tileRow;
			for (int y = 0; y < mapHeight; y++)
			{
				auto spriteTile = spriteLayer->getTileAt(Vec2(x, y));
				if (spriteTile != NULL)
				{
					currentPoint.setPoint(x, y);
					foundStart = true;
					spriteSize = spriteTile->getContentSize();
					break;
				}
			}
		}

		//If there is a point
		while (foundStart)
		{
			//Main Loop

			//Add current point into list
			listOfPoints.push_back(currentPoint);

			//Check currentDir
			while (currentDirection != backDirection)
			{
				//Get next point
				Vec2 nextCheck = currentPoint + directions[currentDirection];
				//Reached Endpoint
				if (nextCheck == listOfPoints.front())
				{
					foundStart = false;
					break;
				}
				//Check next direction
				if (nextCheck.x < bounds.width && nextCheck.y < bounds.height && nextCheck.x >= 0 && nextCheck.y >= 0)
				{
					int gid = spriteLayer->getTileGIDAt(nextCheck);
					//0 = dont have, 1 = have
					if (gid)
					{
						//Found Point
						previousPoint = currentPoint;
						currentPoint = nextCheck;
						backDirection = wrap(currentDirection - 4, 0, 8);
						currentDirection = wrap(currentDirection - 3, 0, 8);
						break;
					}
					else
					{
						currentDirection = wrap(currentDirection + 1, 0, 8);
						continue;
					}
				}
				else
				{
					currentDirection = wrap(currentDirection + 1, 0, 8);
					continue;
				}
			}
			if (currentDirection == backDirection)
				break;
		}

		float widthOffset = tilemap->getContentSize().width * 0.5f / spriteSize.x;
		float heightOffset = tilemap->getContentSize().height * 0.5f / spriteSize.y;

		Vec2* collisionOutline = new Vec2[listOfPoints.size()];
		//std::reverse(listOfPoints.begin(), listOfPoints.end());
		std::copy(listOfPoints.begin(), listOfPoints.end(), collisionOutline);
		for (int i = 0; i < listOfPoints.size(); i++)
		{
			collisionOutline[i].x -= widthOffset;//translating back to origin
			collisionOutline[i].x *= spriteSize.x; //then scale
			collisionOutline[i].x += spriteSize.x * 0.5f; //translate back to point
			//collisionOutline[i].x -= (visibleSize.width * 0.25f);
			collisionOutline[i].y -= heightOffset;
			collisionOutline[i].y *= spriteSize.y;
			collisionOutline[i].y += spriteSize.y * 0.5f;
			collisionOutline[i].y = /*playingSize.height */-collisionOutline[i].y;
		}

		PhysicsBody* tilePhysics = PhysicsBody::createEdgePolygon(collisionOutline, listOfPoints.size(), PhysicsMaterial(1000000.1f, 0.f, 1.0f));
		tilePhysics->setDynamic(false);
		tilePhysics->setCollisionBitmask(LayerPlayer | LayerEnemy);
		tilePhysics->setCategoryBitmask(LayerGround);
		tilePhysics->setContactTestBitmask(LayerPlayer);

		//Node* physicsNode = Node::create();
		//physicsNode->setPhysicsBody(tilePhysics);
		spriteLayer->setPhysicsBody(tilePhysics);
		//tilemap->setPhysicsBody(tilePhysics);
		//tilemap->addChild(physicsNode);
	}
	this->addChild(tilemap);

	//background 
	//auto paraBG = ParallaxNode::create();
	auto paraSprite = Sprite::create("henesysBG.png");
	paraSprite->setName("Background");
	paraSprite->setScale(5);
	paraSprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	paraSprite->setPosition(Vec2::ZERO);
	paraSprite->setZOrder(-10);
	//paraBG->addChild(paraSprite, 1, Vec2(.9f,.9f), Vec2::ZERO);
	this->addChild(paraSprite);


	//Sprite Sheet
	SpriteBatchNode* spriteBatch = SpriteBatchNode::create("sprite.png");
	SpriteFrameCache* cache = SpriteFrameCache::getInstance();
	cache->addSpriteFramesWithFile("sprite.plist");
	AnimationCache::getInstance()->addAnimationsWithFile("sprite_ani.plist");

	//making mushmummy test
	SpriteBatchNode* mushBatch = SpriteBatchNode::create("mushmummySprite.png");
	cache->addSpriteFramesWithFile("mushmummy.plist");
	AnimationCache::getInstance()->addAnimationsWithFile("mushmummy_ani.plist");

	//making gangster test
	SpriteBatchNode* gangBatch = SpriteBatchNode::create("gangsterSprite.png");
	cache->addSpriteFramesWithFile("gangster.plist");
	AnimationCache::getInstance()->addAnimationsWithFile("gangster_ani.plist");

	//getting a reference to the objects layer in tilemap
	TMXObjectGroup* objectGroup = tilemap->objectGroupNamed("Objects");

	//Sprites
	auto spriteNode = Node::create();
	spriteNode->setName("spriteNode");

	//mainsprite
	auto mainSprite = Sprite::create();
	mainSprite->setSpriteFrame(cache->getSpriteFrameByName("mushmummy_idle1.png"));
	mainSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	//mainSprite->setPosition(100, playingSize.height);
	//getting spawnpoint from the tilemap, pos in tilemap + tilemap's pos in prog whole time multiply by scale
	ValueMap spawnPoint = objectGroup->objectNamed("SpawnPoint");
	int xPos = (spawnPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
	int yPos = (spawnPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
	mainSprite->setPosition(xPos, yPos);
	mainSprite->setName("mainSprite");

	Animation* anim = AnimationCache::getInstance()->getAnimation("mushmummy_idle");
	mainSprite->setScaleX(-abs(mainSprite->getScaleX()));
	auto moveAnim = RepeatForever::create(Animate::create(anim));
	moveAnim->setTag(ANIMATION);
	mainSprite->runAction(moveAnim);

	//adding physics body
	/*PhysicsBody* physicsBody = PhysicsBody::createBox(Size(mainSprite->getContentSize().width, mainSprite->getContentSize().height), PhysicsMaterial(.1f, 0.f, 1.f));
	physicsBody->setCategoryBitmask(LayerPlayer);
	physicsBody->setCollisionBitmask(LayerGround);
	physicsBody->setContactTestBitmask(LayerGround | LayerEnemy | LayerTrigger);
	physicsBody->setRotationEnable(false);

	mainSprite->addComponent(physicsBody);*/

	spriteNode->addChild(mainSprite, 1);


	auto player = Player::create();
	player->setName("player");
	player->setSpriteFrame(cache->getSpriteFrameByName("mushmummy_idle1.png"));
	player->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

	/*ValueMap*/ spawnPoint = objectGroup->objectNamed("SpawnPoint");
	/*int*/ xPos = (spawnPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
	/*int*/ yPos = (spawnPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
	player->setPosition(xPos, yPos);

	//adding physics body
	PhysicsBody* physicsBody = PhysicsBody::createBox(player->getContentSize(), PhysicsMaterial(.1f, 0.f, 1.f));
	physicsBody->setCategoryBitmask(LayerPlayer);
	physicsBody->setCollisionBitmask(LayerGround);
	physicsBody->setContactTestBitmask(LayerGround | LayerEnemy | LayerTrigger);
	physicsBody->setRotationEnable(false);
	player->addComponent(physicsBody);

	player->SetAnimNames("mushmummy_walk", "mushmummy_jump", "mushmummy_idle", "mushmummy_dead", "mushmummy_attack", "mushmummy_hurt");
	//player->Idle();

	spriteNode->addChild(player, 1);

	//Test enemy
	auto enemy = Sprite::create();
	enemy->setSpriteFrame(cache->getSpriteFrameByName("gangster_idle1.png"));
	enemy->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	enemy->setPosition(300, playingSize.height);
	enemy->setName("enemySprite");

	Animation* eanim = AnimationCache::getInstance()->getAnimation("gangster_idle");
	enemy->setScaleX(-abs(mainSprite->getScaleX()));
	enemy->stopActionByTag(ANIMATION);
	moveAnim = RepeatForever::create(Animate::create(eanim));
	moveAnim->setTag(ANIMATION);
	enemy->runAction(moveAnim);


	//adding physics body
	PhysicsBody* ephysicsBody = PhysicsBody::createBox(Size(enemy->getContentSize().width, enemy->getContentSize().height), PhysicsMaterial(.1f, 0.f, 0.f));
	ephysicsBody->setCollisionBitmask(LayerGround);
	ephysicsBody->setCategoryBitmask(LayerEnemy);
	ephysicsBody->setContactTestBitmask(LayerGround | LayerPlayer);
	ephysicsBody->setRotationEnable(false);
	enemy->addComponent(ephysicsBody);

	spriteNode->addChild(enemy, 1);

	this->addChild(nodeItems, 1);
	this->addChild(spriteNode, 1);

	//enemySpawner->Init("this/spriteNode/", 1, 1);
	InitializeObjects();

	auto portalSprite = Sprite::create("Portal.png");
	portalSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	portalSprite->setName("Portal");
	/*
	ValueMap portalPoint = objectGroup->objectNamed("SpawnPoint");
	xPos = (portalPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
	yPos = (portalPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
	portalSprite->setPosition(xPos, yPos);
	*/
	ValueMap portalPoint = objectGroup->objectNamed("Portal");
	xPos = (portalPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
	yPos = (portalPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
	portalSprite->setPosition(xPos, yPos);

	//portalSprite->setPosition(100, playingSize.height * 0.75f);
	//adding physics body
	auto portalBody = PhysicsBody::createBox(Size(portalSprite->getContentSize().width, portalSprite->getContentSize().height), PhysicsMaterial(0.1f, 0.5f, 0.0f));

	//portalBody->setCollisionBitmask(LayerGround);
	portalBody->setCategoryBitmask(LayerTrigger);
	portalBody->setContactTestBitmask(LayerPlayer);
	portalBody->setRotationEnable(false);
	portalBody->setDynamic(false);
	portalSprite->addComponent(portalBody);

	spriteNode->addChild(portalSprite, 1);

	for (int i = 0; i < objectGroup->getObjects().size(); i++)
	{
		std::string enemyName = "Enemy" + std::to_string(i + 1);
		spawnPoint = objectGroup->getObject(enemyName);

		if (spawnPoint.cbegin() == spawnPoint.cend())//basically stating if the value map obtained has any values attached to it, since i cant just == null/nullptr
			continue;

		auto enemySprite = Sprite::create();
		enemySprite->setName(enemyName);
		enemySprite->setSpriteFrame(cache->getSpriteFrameByName("gangster_attack1.png"));
		enemySprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

		xPos = (spawnPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
		yPos = (spawnPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
		enemySprite->setPosition(xPos, yPos);

		//enemySprite->setName("enemySprite" + std::to_string(i + 1));
		//switch (rand() % 2)
		//{
		//case 0:
		//anim = AnimationCache::getInstance()->getAnimation("gangster_die");
		//	break;
		//case 1:
		//	//anim = AnimationCache::getInstance()->getAnimation("gangster_");
		//	break;

		//}
		anim = AnimationCache::getInstance()->getAnimation("gangster_attack");
		enemySprite->setScaleX(-abs(enemySprite->getScaleX()));
		moveAnim = RepeatForever::create(Animate::create(anim));
		moveAnim->setTag(ANIMATION);
		enemySprite->runAction(moveAnim);

		//adding physics body
		PhysicsBody* ephysicsBody = PhysicsBody::createBox(Size(enemySprite->getContentSize().width, enemySprite->getContentSize().height), PhysicsMaterial(.1f, 0.f, 0.f));
		ephysicsBody->setCollisionBitmask(LayerGround);
		ephysicsBody->setCategoryBitmask(LayerEnemy);
		ephysicsBody->setContactTestBitmask(LayerGround | LayerPlayer);
		ephysicsBody->setRotationEnable(false);
		enemySprite->addComponent(ephysicsBody);

		spriteNode->addChild(enemySprite);
	}

	//Events
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(HelloWorld2::onContactBegin, this);
	contactListener->onContactSeparate = CC_CALLBACK_1(HelloWorld2::onContactSeperate, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(HelloWorld2::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(HelloWorld2::onKeyReleased, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	//Touch Event
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = [](Touch* touch, Event* event)
	{
		return true;
	};
	touchListener->onTouchMoved = [](Touch* touch, Event* event)
	{
		return true;
	};
	touchListener->onTouchEnded = [](Touch* touch, Event* event)
	{
		return true;
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	//-------------------------------Create buttons
	//Moving Left Button
	auto moveLeftButton = ui::Button::create("LeftButtonUp.png", "LeftButtonDown.png", "LeftButtonDown.png");
	moveLeftButton->setPosition(Vec2(playingSize.width * 0.1, (playingSize.height * 0.2f)));
	moveLeftButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld2::LeftButtonResponse, this));

	//Moving Right Button
	auto moveRightButton = ui::Button::create("RightButtonUp.png", "RightButtonDown.png", "RightButtonDown.png");
	moveRightButton->setPosition(Vec2(playingSize.width * 0.3, (playingSize.height * 0.2f)));
	moveRightButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld2::RightButtonResponse, this));

	//Jumping Button
	auto jumpButton = ui::Button::create("UpButtonUp.png", "UpButtonDown.png", "UpButtonDown.png");
	jumpButton->setPosition(Vec2(playingSize.width * 0.85, (playingSize.height * 0.2f)));
	jumpButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld2::JumpButtonResponse, this));

	//Interact Button
	auto interactButton = ui::Button::create("UpButtonUp.png", "UpButtonDown.png", "UpButtonDown.png");
	interactButton->setPosition(Vec2(playingSize.width * 0.9, (playingSize.height * 0.3f)));
	interactButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld2::InteractButtonResponse, this));
	interactButton->setScale(.5f);

	//Back Button
	auto backButton = ui::Button::create("LeftButtonUp.png", "LeftButtonDown.png", "LeftButtonDown.png");
	backButton->setPosition(Vec2(playingSize.width * 0.05, (playingSize.height * 1.f)));
	backButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld2::BackButtonResponse, this));
	backButton->setScale(.7f);

	this->addChild(moveLeftButton, 1);
	this->addChild(moveRightButton, 1);
	this->addChild(jumpButton, 1);
	this->addChild(interactButton, 1);
	this->addChild(backButton, 1);

	//-------------------------------Camera masks
	//Director::getInstance()->getRunningScene()->setCameraMask((unsigned short)CameraFlag::USER1);
	this->setCameraMask((unsigned short)CameraFlag::USER1);

	moveLeftButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	moveRightButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	jumpButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	interactButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	backButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	paraSprite->setCameraMask((unsigned short)CameraFlag::USER1);

	//log("camera mask: %i", Camera::getDefaultCamera()->getCameraMask());
	//log("scene mask: %i",this->getCameraMask());
	//log("left mask: %i", moveLeftButton->getCameraMask());
	//log("right mask: %i", moveRightButton->getCameraMask());


	return true;
}


void HelloWorld2::menuCloseCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}


void HelloWorld2::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_SPACE:
		//Director::getInstance()->pushScene(EmptyScene::createScene());
		Director::getInstance()->pushScene(HelloWorld::createScene());
		break;
	}
	//old movement
	//if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
	//{
	//	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	//	auto moveEvent = MoveBy::create(.5f, Vec2(200, 0));
	//	auto repeatEvent = RepeatForever::create(moveEvent);
	//	repeatEvent->setTag(1);
	//	curSprite->runAction(repeatEvent);
	//}
	//else if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
	//{
	//	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	//	auto moveEvent = MoveBy::create(.5f, Vec2(-200, 0));
	//	auto repeatEvent = RepeatForever::create(moveEvent);
	//	repeatEvent->setTag(2);
	//	curSprite->runAction(repeatEvent);
	//}
}

void HelloWorld2::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	//if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
	//{
	//	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	//	curSprite->stopActionByTag(1);
	//}
	//else if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
	//{
	//	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	//	curSprite->stopActionByTag(2);
	//}
}

void HelloWorld2::onMouseMove(Event* event)
{
	EventMouse* e = (EventMouse*)event;
}


void HelloWorld2::onMouseUp(Event* event)
{
	EventMouse* e = (EventMouse*)event;
	//e->getMouseButton();

	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	Vector<FiniteTimeAction*> arrayOfActions;

	//!!!TODO change this part to make it be physics based instead of move to 
	arrayOfActions.pushBack(MoveTo::create(2, mousePos));
	arrayOfActions.pushBack(CallFunc::create(
		[]()
		{
			auto scene = Director::getInstance()->getRunningScene();
			auto thisScene = scene->getChildByTag(999);
			thisScene->getChildByName("spriteNode")->getChildByName("mainSprite")->stopActionByTag(ANIMATION);
		}
	));
	auto moveEvent = Sequence::create(arrayOfActions);
	moveEvent->setTag(MOVEMENT);

	auto dir = mousePos - curSprite->getPosition();
	//Vector<SpriteFrame*> animFrames;
	//animFrames.reserve(4);
	Animation* anim = nullptr;

	if (abs(dir.x) > abs(dir.y))
	{
		//Left Right
		if (dir.x > 0) //right need reverse scale due to main sprite facing left
		{
			//anim = AnimationCache::getInstance()->getAnimation("walk_right");
			anim = AnimationCache::getInstance()->getAnimation("mushmummy_idle");
			curSprite->setScaleX(-abs(curSprite->getScaleX()));
		}
		else
		{
			anim = AnimationCache::getInstance()->getAnimation("mushmummy_idle");
			curSprite->setScaleX(abs(curSprite->getScaleX()));
		}

	}
	else
	{
		anim = AnimationCache::getInstance()->getAnimation("mushmummy_attack");
	}

	curSprite->stopActionByTag(MOVEMENT);
	curSprite->runAction(moveEvent);

	curSprite->stopActionByTag(ANIMATION);
	auto moveAnim = RepeatForever::create(Animate::create(anim));
	moveAnim->setTag(ANIMATION);
	curSprite->runAction(moveAnim);
}

void HelloWorld2::onMouseDown(Event* event)
{
	EventMouse* e = (EventMouse*)event;

	mousePos.x = e->getCursorX();
	mousePos.y = e->getCursorY();
}

void HelloWorld2::onMouseScroll(cocos2d::Event * event)
{
}

#pragma region Button Responses
void HelloWorld2::LeftButtonResponse(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
	auto player = (Player*)(this->getChildByName("spriteNode")->getChildByName("player"));
	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		player->MoveLeft();
		b_leftButtonHold = true;
		break;
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
		player->Idle();
		b_leftButtonHold = false;
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		b_leftButtonHold = false;
		player->Idle();
		break;
	default:
		break;
	}
}
void HelloWorld2::RightButtonResponse(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
	auto player = (Player*)(this->getChildByName("spriteNode")->getChildByName("player"));
	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		player->MoveRight();
		b_rightButtonHold = true;
		//PlayerMoveRight();
		break;
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
		player->Idle();
		b_rightButtonHold = false;
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		b_rightButtonHold = false;
		player->Idle();
		break;
	default:
		break;
	}
}
void HelloWorld2::JumpButtonResponse(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		b_upButtonHold = true;
		//PlayerJump();
		break;
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
		b_upButtonHold = false;
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		b_upButtonHold = false;
		break;
	default:
		break;
	}
}
void HelloWorld2::InteractButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type)
{
	if (onPortal)
	{
		//one day we will save the scene
		Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.5f, HelloWorld::createScene()));
	}
}
void HelloWorld2::BackButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type)
{
	Director::getInstance()->replaceScene(EmptyScene::createScene());
}
bool HelloWorld2::onContactBegin(PhysicsContact& begin)
{
	auto bodyA = begin.getShapeA()->getBody();
	auto bodyB = begin.getShapeB()->getBody();
	//Check if is lower layer
	if (bodyB->getCategoryBitmask() < bodyA->getCategoryBitmask())
	{
		//Swap
		auto temp = bodyB;
		bodyB = bodyA;
		bodyA = temp;
	}

	//Player collide with a trigger
	if (bodyA->getCategoryBitmask() == LayerPlayer && bodyB->getCategoryBitmask() == LayerTrigger)
	{
		if (bodyB->getOwner()->getName() == "Portal")
		{
			onPortal = true;
		}
	}

	//Player collide with ground
	if (bodyA->getCategoryBitmask() == LayerPlayer && bodyB->getCategoryBitmask() == LayerGround)
	{
		isGrounded = true;
	}
	//Player collide with enemy
	if (bodyA->getCategoryBitmask() == LayerPlayer && bodyB->getCategoryBitmask() == LayerEnemy)
	{
		Node* owner = bodyB->getOwner();
		//making animation
		//curSprite->setScaleX(abs(curSprite->getScaleX())); //aligning sprite to correct direction (default- left)
		auto passawayAnim = Animate::create(AnimationCache::getInstance()->getAnimation("gangster_die"));
		//making an array of actions
		Vector<FiniteTimeAction*> arrayOfActions;
		arrayOfActions.pushBack(passawayAnim);
		arrayOfActions.pushBack(CallFunc::create(
			[owner]()
			{
				owner->removeFromParentAndCleanup(true);
				/*
				auto scene = Director::getInstance()->getRunningScene();
				auto thisScene = scene->getChildByTag(999);
				//thisScene->getChildByName("spriteNode")->getChildByName("mainSprite")->stopActionByTag(ANIMATION);
				thisScene->child
				*/
			}
		));
		auto fulldeadEvent = Sequence::create(arrayOfActions);
		owner->stopAllActions();
		owner->runAction(fulldeadEvent);
		bodyB->setEnabled(false);
	}
	//log(bodyA->getOwner()->getName().c_str());
	log("body 1: %d, body 2: %d", bodyA->getCategoryBitmask(), bodyB->getCategoryBitmask());

	return true;
}

//On Contact End
bool HelloWorld2::onContactSeperate(cocos2d::PhysicsContact & begin)
{
	auto bodyA = begin.getShapeA()->getBody();
	auto bodyB = begin.getShapeB()->getBody();
	//Check if is lower layer
	if (bodyB->getCategoryBitmask() < bodyA->getCategoryBitmask())
	{
		//Swap
		auto temp = bodyB;
		bodyB = bodyA;
		bodyA = temp;
	}

	//Player collide with a trigger
	if (bodyA->getCategoryBitmask() == LayerPlayer && bodyB->getCategoryBitmask() == LayerTrigger)
	{
		if (bodyB->getOwner()->getName() == "Portal")
		{
			onPortal = false;
		}
	}

	return true;
}
#pragma endregion

void HelloWorld2::PlayerMoveLeft()
{
	//getting current sprite
	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	curSprite->getPhysicsBody()->setVelocity(Vec2(-250, curSprite->getPhysicsBody()->getVelocity().y));

	//Check if hold or begin
	if (!b_leftButtonHold)
	{
		Animation* anim;
		anim = AnimationCache::getInstance()->getAnimation("mushmummy_walk");
		curSprite->setScaleX(abs(curSprite->getScaleX())); //aligning sprite to correct direction (default- left)
		auto moveAnim = RepeatForever::create(Animate::create(anim));
		moveAnim->setTag(ANIMATION);

		curSprite->stopActionByTag(ANIMATION);
		curSprite->runAction(moveAnim);
	}
	//making movement action
	//auto moveEvent = MoveBy::create(.5f, Vec2(-200, 0));
	//auto repeatEvent = RepeatForever::create(moveEvent);
	//repeatEvent->setTag(MOVEMENT);
	//curSprite->runAction(repeatEvent);

	//!!!TODO change this part to make it be physics based instead of move to 
	//making an array of actions
	//Vector<FiniteTimeAction*> arrayOfActions;
	//arrayOfActions.pushBack(repeatEvent);
	//arrayOfActions.pushBack(CallFunc::create(
	//	[]()
	//	{
	//		auto scene = Director::getInstance()->getRunningScene();
	//		auto thisScene = scene->getChildByTag(999);
	//		thisScene->getChildByName("spriteNode")->getChildByName("mainSprite")->stopActionByTag(ANIMATION);
	//	}
	//));
	//auto fullMoveEvent = Sequence::create(arrayOfActions);
	//fullMoveEvent->setTag(MOVEMENT);


	//running the actions
	//curSprite->stopActionByTag(MOVEMENT);
	//curSprite->runAction(repeatEvent);

}
void HelloWorld2::PlayerMoveRight()
{
	//getting current sprite
	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	curSprite->getPhysicsBody()->setVelocity(Vec2(250, curSprite->getPhysicsBody()->getVelocity().y));
	//curSprite->getPhysicsBody()->applyForce(Vec2(2500000, 0));

	//Check hold or begin
	if (!b_rightButtonHold)
	{
		//making animation
		Animation* anim;
		anim = AnimationCache::getInstance()->getAnimation("mushmummy_walk");
		curSprite->setScaleX(-abs(curSprite->getScaleX())); //aligning sprite to correct direction (default- left)
		auto moveAnim = RepeatForever::create(Animate::create(anim));
		moveAnim->setTag(ANIMATION);

		//running the actions
		//curSprite->stopActionByTag(MOVEMENT);
		//curSprite->runAction(repeatEvent);

		curSprite->stopActionByTag(ANIMATION);
		curSprite->runAction(moveAnim);
	}

	//making movement action
	/*auto moveEvent = MoveBy::create(.5f, Vec2(200, 0));
	auto repeatEvent = RepeatForever::create(moveEvent);
	repeatEvent->setTag(MOVEMENT);*/

	//!!!TODO change this part to make it be physics based instead of move to 
	//making an array of actions
	/*
	Vector<FiniteTimeAction*> arrayOfActions;
	arrayOfActions.pushBack(repeatEvent);
	arrayOfActions.pushBack(CallFunc::create(
		[]()
		{
			auto scene = Director::getInstance()->getRunningScene();
			auto thisScene = scene->getChildByTag(999);
			thisScene->getChildByName("spriteNode")->getChildByName("mainSprite")->stopActionByTag(ANIMATION);
		}
	));

	auto fullMoveEvent = Sequence::create(arrayOfActions);
	fullMoveEvent->setTag(MOVEMENT);
	*/
}

void HelloWorld2::PlayerJump()
{
	//if (!isGrounded)
	//	return;
	isGrounded = false;
	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	//curSprite->getPhysicsBody()->applyImpulse(Vec2(0, 600000));
	curSprite->getPhysicsBody()->setVelocity(Vec2(0, 200));
	//making animation
	Animation* anim;
	anim = AnimationCache::getInstance()->getAnimation("mushmummy_attack");
	auto moveAnim = RepeatForever::create(Animate::create(anim));
	moveAnim->setTag(ANIMATION);

	//INSERT JUMP PHYSICS HERE, theres maybe some logic in button manager
	////running the actions
	//curSprite->stopActionByTag(MOVEMENT);
	//curSprite->runAction(moveEvent);

	curSprite->stopActionByTag(ANIMATION);
	curSprite->runAction(moveAnim);

}
void HelloWorld2::PlayerStop()
{
	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	curSprite->getPhysicsBody()->setVelocity(Vec2::ZERO);
	//curSprite->stopActionByTag(MOVEMENT);

	//making animation
	Animation* anim;
	anim = AnimationCache::getInstance()->getAnimation("mushmummy_idle");
	//curSprite->setScaleX(-abs(curSprite->getScaleX())); //aligning sprite to correct direction (default- left)
	auto moveAnim = RepeatForever::create(Animate::create(anim));
	moveAnim->setTag(ANIMATION);
	curSprite->stopActionByTag(ANIMATION);
	curSprite->runAction(moveAnim);

}
void HelloWorld2::update(float dt)
{
	SceneBase::update(dt);

	//auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");

	Sprite* bgSprite = (Sprite*)(this->getChildByName("Background"));

	auto player = (Player*)(this->getChildByName("spriteNode")->getChildByName("player"));
	Vec2 targetCamPos = player->getPosition();

	Vec2 camBounds = Vec2(
		bgSprite->getContentSize().width * bgSprite->getScaleX() - Director::getInstance()->getVisibleSize().width * .5f,
		bgSprite->getContentSize().height * bgSprite->getScaleY() + Director::getInstance()->getVisibleSize().height * .5f
	);
	//Check X
	targetCamPos.x = clamp(targetCamPos.x, camBounds.x* 0.5, bgSprite->getPositionX() + camBounds.x);
	//Check Y
	targetCamPos.y = clamp(targetCamPos.y, camBounds.y* 0.25, bgSprite->getPositionY() + camBounds.y);

	worldCamera->setPosition(targetCamPos);

	if (b_rightButtonHold)
	{
		player->MoveRight();
	}
	else if (b_leftButtonHold)
	{
		player->MoveLeft();
	}
	else if (b_upButtonHold)
	{
		player->Jump();
	}
	/*
	log("camera mask: %i", Camera::getDefaultCamera()->getCameraMask());
	log("scene mask: %i", this->getCameraMask());
	log("left mask: %i", moveLeftButton->getCameraMask());
	log("right mask: %i", moveLeftButton->getCameraMask());
	*/
}

//void HelloWorld2::SpawnEnemies()
//{
//	auto tilemap = this->getChildByName("Tilemap");
//
//	//getting a reference to the objects layer in tilemap
//	TMXObjectGroup* objectGroup = (dynamic_cast<TMXTiledMap*>(tilemap))->objectGroupNamed("Objects");
//	ValueMap spawnPoint;
//
//	for (int i = 0; i < objectGroup->getObjects().size(); i++)
//	{
//		std::string enemyName = "Enemy" + std::to_string(i + 1);
//		spawnPoint = objectGroup->getObject(enemyName);
//
//		if (spawnPoint.cbegin() == spawnPoint.cend())//basically stating if the value map obtained has any values attached to it, since i cant just == null/nullptr
//			continue;
//
//		if (this->getChildByName(enemyName) != nullptr)
//			continue;
//
//
//		auto enemySprite = Sprite::create();
//		enemySprite->setName(enemyName);
//		enemySprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("gangster_idle1.png"));
//		enemySprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
//
//		int xPos = (spawnPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
//		int yPos = (spawnPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
//		enemySprite->setPosition(xPos, yPos);
//
//		//enemySprite->setName("enemySprite" + std::to_string(i + 1));
//		//switch (rand() % 2)
//		//{
//		//case 0:
//		//anim = AnimationCache::getInstance()->getAnimation("gangster_die");
//		//	break;
//		//case 1:
//		//	//anim = AnimationCache::getInstance()->getAnimation("gangster_");
//		//	break;
//
//		//}
//		Animation* anim = AnimationCache::getInstance()->getAnimation("gangster_idle");
//		enemySprite->setScaleX(-abs(enemySprite->getScaleX()));
//		auto moveAnim = RepeatForever::create(Animate::create(anim));
//		moveAnim->setTag(ANIMATION);
//		enemySprite->runAction(moveAnim);
//
//		//adding physics body
//		PhysicsBody* ephysicsBody = PhysicsBody::createBox(Size(enemySprite->getContentSize().width, enemySprite->getContentSize().height), PhysicsMaterial(.1f, 0.f, 0.f));
//		ephysicsBody->setCollisionBitmask(LayerGround);
//		ephysicsBody->setCategoryBitmask(LayerEnemy);
//		ephysicsBody->setContactTestBitmask(LayerGround | LayerPlayer);
//		ephysicsBody->setRotationEnable(false);
//		enemySprite->addComponent(ephysicsBody);
//		enemySprite->setCameraMask((unsigned short)CameraFlag::USER1);
//		this->getChildByName("spriteNode")->addChild(enemySprite);
//
//	}
//}

/*
void HelloWorld2::onMouseScroll(Event * event)
{
	EventMouse* e = (EventMouse*)event;

	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	auto moveEvent = MoveBy::create(0.f, Vec2(0, -e->getScrollY()));
	curSprite->runAction(moveEvent);
}
*/