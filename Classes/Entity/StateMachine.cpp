#include "StateMachine.h"
#include "Enemy.h"

//StateMachine* StateMachine::instance = nullptr;

//StateMachine* StateMachine::getInstance()
//{
//	if (instance == nullptr)
//	{
//		instance = new StateMachine();
//	}
//	return instance;
//}

StateMachine::StateMachine()
{
}

StateMachine::~StateMachine()
{
}

void StateMachine::Update(float dt, Enemy* enemy)
{
	//if current state is diff from next state, change state

	if (enemy->curState != nullptr && enemy->nextState != nullptr)
	{
		enemy->curState->Update(dt, enemy);

		if (enemy->curState != enemy->nextState)
		{
			enemy->curState->Exit(enemy);
			enemy->curState = enemy->nextState;
			enemy->curState->Enter(enemy);
			enemy->curState->Update(dt, enemy);
			return;
		}

	}
	//switch (enemy->curState)
	//{
	//case Enemy::STATE_IDLE:
	//	//play an animation 
	//	break;
	//case Enemy::STATE_MOVE_LEFT:
	//	//move left
	//	break;
	//case Enemy::STATE_MOVE_RIGHT:
	//	//move right
	//	break;
	//default:
	//	break;
	//}
}

void StateMachine::AddState(std::string stateName, State* newState)
{
	stateMap.insert(std::pair<std::string, State*>(stateName, newState));
}

void StateMachine::SetNextState(Enemy* enemy, const std::string& nextStateID)
{
	std::map<std::string, State*>::iterator state = stateMap.find(nextStateID);

	if (state != stateMap.end())
	{
		if (enemy->curState == nullptr)
		{
			enemy->curState = (State*)state->second;
		}
		enemy->nextState = (State*)state->second;
	}

}

const std::string& StateMachine::GetCurrentState(Enemy* enemy)
{
	// TODO: insert return statement here
	return enemy->curState->GetName();

}

StateMachine * StateMachine::create()
{
	//copied from sprite
	StateMachine* stateMachine = new (std::nothrow) StateMachine();
	if (stateMachine)
	{
		stateMachine->autorelease(); //could delete away this line if we want to carry it between scenes
		return stateMachine;
	}
	CC_SAFE_DELETE(stateMachine);
	return nullptr;
}

