#include "Entity.h"
#include "../Utility.h"
#include "Player.h"

using namespace Utility;
USING_NS_CC;
Entity::Entity()
{
	Sprite::Sprite();
	this->setScaleY(-this->getScaleY());
}

Entity::~Entity()
{
}

void Entity::MoveLeft()
{
	//set vel stuffs
	this->getPhysicsBody()->setVelocity(Vec2(-250, this->getPhysicsBody()->getVelocity().y));
	//if prev action is not this action, play an animation
	if (prevAction != ACT_MOVELEFT)
	{
		Animation* anim;
		anim = AnimationCache::getInstance()->getAnimation(moveAnimName);
		this->setScaleX(-abs(this->getScaleX())); //aligning sprite to correct direction (default- left)
		auto moveAnim = RepeatForever::create(Animate::create(anim));
		moveAnim->setTag(ANIMATION);

		this->stopActionByTag(ANIMATION);
		this->runAction(moveAnim);
		prevAction = ACT_MOVELEFT;
	}
}

void Entity::MoveRight()
{
	//set vel stuffs
	this->getPhysicsBody()->setVelocity(Vec2(250, this->getPhysicsBody()->getVelocity().y));
	//if prev action is not this action, play an animation
	if (prevAction != ACT_MOVERIGHT)
	{
		Animation* anim;
		anim = AnimationCache::getInstance()->getAnimation(moveAnimName);
		this->setScaleX(abs(this->getScaleX())); //aligning sprite to correct direction (default- left)
		auto moveAnim = RepeatForever::create(Animate::create(anim));
		moveAnim->setTag(ANIMATION);

		this->stopActionByTag(ANIMATION);
		this->runAction(moveAnim);
		prevAction = ACT_MOVERIGHT;
	}
}

void Entity::Jump()
{
	if (!isGrounded)
		return;

	isGrounded = false;
	this->getPhysicsBody()->setVelocity(Vec2(this->getPhysicsBody()->getVelocity().x, 300));

	if (prevAction != ACT_JUMP)
	{
		//making animation
		Animation* anim;
		anim = AnimationCache::getInstance()->getAnimation(jumpAnimName);
		auto moveAnim = RepeatForever::create(Animate::create(anim));
		moveAnim->setTag(ANIMATION);

		//INSERT JUMP PHYSICS HERE, theres maybe some logic in button manager
		////running the actions
		//curSprite->stopActionByTag(MOVEMENT);
		//curSprite->runAction(moveEvent);

		this->stopActionByTag(ANIMATION);
		this->runAction(moveAnim);
		prevAction = ACT_JUMP;
	}
}

void Entity::Die()
{
	auto passawayAnim = Animate::create(AnimationCache::getInstance()->getAnimation(deadAnimName));
	//making an array of actions
	Vector<FiniteTimeAction*> arrayOfActions;
	arrayOfActions.pushBack(passawayAnim);
	arrayOfActions.pushBack(CallFunc::create(
		[this]()
		{
			/*
			auto thisScene = (HelloWorld*)(scene->getChildByTag(999));
			*/
			this->removeFromParentAndCleanup(true);
		}
	));

	//earnCurrency
	cocos2d::Sprite* earnCurrency;
	earnCurrency = cocos2d::Sprite::create("Shop/earnCurrency.png");
	earnCurrency->setPosition(this->getPosition());
	//auto scene = Director::getInstance()->getRunningScene();
	auto scene = this->getScene();
	scene->addChild(earnCurrency);
	++(((Player*)scene->getChildByTag(999)->getChildByName("spriteNode")->getChildByName("player"))->currency);
	earnCurrency->setCameraMask((unsigned short)CameraFlag::USER1);
	//earnCurrency->removeFromParentAndCleanup(true);
	Vector<FiniteTimeAction*> currencyActions;
	currencyActions.pushBack(MoveTo::create(1.0f, earnCurrency->getPosition() + Vec2(0, 50)));
	currencyActions.pushBack(DelayTime::create(0.25f));
	currencyActions.pushBack(CallFunc::create(
		[earnCurrency]()
		{
			earnCurrency->removeFromParentAndCleanup(true);
		}
	));
	//;
	auto currencyEvents = Sequence::create(currencyActions);
	earnCurrency->stopAllActions();
	earnCurrency->runAction(currencyEvents);

	auto fulldeadEvent = Sequence::create(arrayOfActions);
	this->stopAllActions();
	this->runAction(fulldeadEvent);
}

void Entity::Idle()
{
	this->getPhysicsBody()->setVelocity(Vec2::ZERO);
	if (prevAction != ACT_IDLE)
	{
		//making animation
		Animation* anim;
		anim = AnimationCache::getInstance()->getAnimation(idleAnimName);
		auto moveAnim = RepeatForever::create(Animate::create(anim));
		moveAnim->setTag(ANIMATION);
		this->stopActionByTag(ANIMATION);
		this->runAction(moveAnim);
		prevAction = ACT_IDLE;
	}
}

void Entity::SetAnimNames(std::string moveAnim, std::string jumpAnim, std::string idleAnim, std::string deadAnim, std::string attackAnim, std::string hurtAnim)
{
	this->moveAnimName = moveAnim;
	this->jumpAnimName = jumpAnim;
	this->idleAnimName = idleAnim;
	this->deadAnimName = deadAnim;
	this->attackAnimName = attackAnim;
	this->hurtAnimName = hurtAnim;
}

Entity * Entity::create()
{
	//copied from sprite
	Entity* entity = new (std::nothrow) Entity();
	if (entity)
	{
		entity->autorelease(); //could delete away this line if we want to carry it between scenes
		return entity;
	}
	CC_SAFE_DELETE(entity);
	return nullptr;
}
