#include "Player.h"
#include "Utility.h"

#include "SimpleAudioEngine.h"

Player::Player()
	: currency(0)
{
	Entity::Entity();
}


Player::~Player()
{
}

void Player::MoveLeft()
{
	if(!onRope)
		Entity::MoveLeft();
}
void Player::MoveRight()
{
	if (!onRope)
		Entity::MoveRight();
}

void Player::Jump()
{
	UnhangRope();
using namespace cocos2d;
using namespace Utility;

	if (!isGrounded && !onRope)
	{
		return;
	}
	isGrounded = false;
	this->getPhysicsBody()->setVelocity(Vec2(this->getPhysicsBody()->getVelocity().x, 300));

	if (prevAction != ACT_JUMP)
	{
		//making animation
		Animation* anim;
		anim = AnimationCache::getInstance()->getAnimation(jumpAnimName);
		auto moveAnim = RepeatForever::create(Animate::create(anim));
		moveAnim->setTag(ANIMATION);


		//INSERT JUMP PHYSICS HERE, theres maybe some logic in button manager
		////running the actions
		//curSprite->stopActionByTag(MOVEMENT);
		//curSprite->runAction(moveEvent);

		this->stopActionByTag(ANIMATION);
		this->runAction(moveAnim);
		prevAction = ACT_JUMP;
	}
}

void Player::MoveUp()
{
using namespace Utility;
	//Only matters if on a rope
	if (!touchingRope)
		return;

	if (!onRope)
	{
		//Attach to rope

		onRope = true;
		cocos2d::log("On Rope");
		cocos2d::PhysicsBody* pBody = (cocos2d::PhysicsBody*)(this->getComponent("physicsBody"));
		pBody->setGravityEnable(false);
		pBody->setVelocity(cocos2d::Vec2::ZERO);
		this->setPosition(cocos2d::Vec2(ropeXPos, this->getPosition().y + 1));

		//Change the sprite
		this->setSpriteFrame(cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("leader_jump_1.png"));
		this->stopActionByTag(ActionTags::ANIMATION);
		this->getPhysicsBody()->setCollisionBitmask(0);
		return;
	}

	this->setPosition(cocos2d::Vec2(ropeXPos, this->getPosition().y + 2));
}

void Player::MoveDown()
{
using namespace Utility;
	//Only matters if on a rope
	if (!touchingRope)
	{
		//Muh crouch
		return;
	}

	if (!onRope)
	{
		//Attach to rope

		onRope = true;
		flashJump = true;
		cocos2d::log("On Rope");
		cocos2d::PhysicsBody* pBody = (cocos2d::PhysicsBody*)(this->getComponent("physicsBody"));
		pBody->setGravityEnable(false);
		pBody->setVelocity(cocos2d::Vec2::ZERO);
		this->setPosition(cocos2d::Vec2(ropeXPos, this->getPosition().y + 1));

		//Change the sprite
		this->setSpriteFrame(cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("leader_jump_1.png"));
		this->stopActionByTag(ActionTags::ANIMATION);
		this->getPhysicsBody()->setCollisionBitmask(0);
		return;
	}

	this->setPosition(cocos2d::Vec2(ropeXPos, this->getPosition().y - 2));
}

void Player::FlashJump()
{
	if (!flashJumpUnlocked)
		return;
	//Flash jump
	if (!flashJump || onRope || isGrounded)
		return;
	else
	{
using namespace cocos2d;
		//Sprite of flash jump
		this->getPhysicsBody()->setVelocity(cocos2d::Vec2(this->getScaleX() * 700, 300));
		flashJump = false;

		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Audio/Gun.wav");

		//Spawn flash jump animation
		auto fJump = Sprite::create();
		fJump->setScaleX(this->getScaleX());
		//Set position to sprite + take into account of offset - fJump animation offset needs to be 0.5, 0.5
		fJump->setPosition(Vec2(getPosition().x, getPosition().y - this->getContentSize().height ));
		fJump->setScaleX(- this->getScaleX());
		fJump->setCameraMask(this->getCameraMask());

		this->getParent()->addChild(fJump, 1);

		auto fJumpAnim = Animate::create(AnimationCache::getInstance()->getAnimation("flash_jump"));
		//making an array of actions
		Vector<FiniteTimeAction*> arrayOfActions;
		arrayOfActions.pushBack(fJumpAnim);
		arrayOfActions.pushBack(CallFunc::create(
			[fJump]()
		{
			/*
			auto scene = Director::getInstance()->getRunningScene();
			auto thisScene = (HelloWorld*)(scene->getChildByTag(999));
			*/
			fJump->removeFromParentAndCleanup(true);
		}
		));
		auto fulldeadEvent = Sequence::create(arrayOfActions);
		fJump->runAction(fulldeadEvent);

		log("fJump: %f", fJump->getPositionY());
		log("player: %f", this->getPositionY());

		return;
	}
}

void Player::UnhangRope()
{
	//If no longer touching rope, confirm not on Rope, reset values
	this->onRope = false;
	//this->ropeXPos = INT_MIN;	//Some error value
	this->getPhysicsBody()->setGravityEnable(true);

	using namespace Utility;
	//Change the sprite
	this->getPhysicsBody()->setCollisionBitmask(LayerGround);

using namespace cocos2d;
	//making animation
	Animation* anim;
	anim = AnimationCache::getInstance()->getAnimation(idleAnimName);
	auto moveAnim = RepeatForever::create(Animate::create(anim));
	moveAnim->setTag(ANIMATION);
	this->stopActionByTag(ANIMATION);
	this->runAction(moveAnim);
	prevAction = ACT_IDLE;
}

void Player::UpdateOnPortal(bool onPortal)
{
	this->onPortal = onPortal;
}

Player * Player::create()
{
	//copied from sprite
	Player* player = new (std::nothrow) Player();
	if (player)
	{		
		player->autorelease(); //could delete away this line if we want to carry it between scenes
		return player;
	}
	CC_SAFE_DELETE(player);
	return nullptr;
}

