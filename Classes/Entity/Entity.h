#ifndef _ENTITY_H_
#define _ENTITY_H_

#include "cocos2d.h"
#include <string>
#include <stdlib.h>     
class Entity : public cocos2d::Sprite
{
protected:
	std::string moveAnimName;
	std::string jumpAnimName;
	std::string idleAnimName;
	std::string deadAnimName;
	std::string attackAnimName;
	std::string hurtAnimName;

public:
	Entity();
	~Entity();
	
	virtual void Idle();
	virtual	void MoveLeft();
	virtual	void MoveRight();
	virtual	void Jump();

	virtual void Die();

	virtual void SetAnimNames(std::string moveAnim, std::string jumpAnim, std::string idleAnim, std::string deadAnim, std::string attackAnimName, std::string hurtAnimName);
	//yes horrible code, but ya
	enum ACTIONS
	{
		ACT_IDLE,
		ACT_MOVELEFT,
		ACT_MOVERIGHT,
		ACT_JUMP,		
		ACT_SKILL,
	};

	ACTIONS prevAction;

	bool isGrounded;

	static Entity* create();
};

#endif // _ENTITY_H_
