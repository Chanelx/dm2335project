#include "SkillComponent.h"
#include "Player.h"
#include "Utility.h"
#include "SimpleAudioEngine.h"


const std::string SkillComponent::lazer = "pianus_lazer1.png";

SkillComponent::SkillComponent()
{
	Component::Component();
}


SkillComponent::~SkillComponent()
{
}


void SkillComponent::update(float delta)
{
	Component::update(delta);

	if (maxCooldown <= -1)
		return;

	if (onCooldown)
	{
		cooldown -= delta;
		if (cooldown <= 0.f)
		{
			cooldown = maxCooldown;
			onCooldown = false;
		}
	}
}

bool SkillComponent::init()
{
	maxCooldown = -1;
	cooldown = -1;
	onCooldown = true;
	return true;
}

bool SkillComponent::init(float maxCooldown)
{
	this->maxCooldown = maxCooldown;
	this->cooldown = maxCooldown;
	this->onCooldown = false;

	return true;
}

void SkillComponent::SetSkill(Skills skill)
{
	currentSkill = skill;
	//Change the image and shit
	//cocos2d::Sprite* skillIcon = (cocos2d::Sprite*)(this->getOwner()->getChildByName("Skill Icon"));
	cocos2d::ui::Button* skillIcon = (cocos2d::ui::Button*)(this->getOwner());

	switch (currentSkill)
	{
	case Skills::NothingPersonalKid:
		skillIcon->loadTextures("dankuusenSkill.png", "dankuusenSkillDown.png");
		break;
	case Skills::NONE:
	default:
		skillIcon->loadTextures("blankSkill.png", "blankSkillDown.png");
		break;
	}
}

SkillComponent * SkillComponent::create()
{
	//copied from Component
	SkillComponent * ret = new (std::nothrow) SkillComponent();

	if (ret && ret->init())
	{
		ret->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(ret);
	}

	return ret;
}

SkillComponent * SkillComponent::create(float maxCooldown)
{
	//copied from Component
	SkillComponent * ret = new (std::nothrow) SkillComponent();

	if (ret && ret->init(maxCooldown))
	{
		ret->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(ret);
	}

	return ret;
}

void SkillComponent::ActivateSkill(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
	//Temp for testing and debugging
	//if(type == cocos2d::ui::Widget::TouchEventType::BEGAN)
	//	SetSkill(Skills::NothingPersonalKid);
	switch (currentSkill)
	{
	case Skills::NothingPersonalKid:
		//Animation Lock
	{
using namespace cocos2d;
CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Audio/Weapon.wav");

		//Find the player somehow, like wtf cocos
		Player* player = (Player*)(this->getOwner()->getParent()->getChildByName("spriteNode")->getChildByName("player"));
		if (player->skillLock)
			return;

		static float travelDistance = 500.f;

		player->skillLock = true;
		player->prevAction = Entity::ACTIONS::ACT_SKILL;

		//Animation of attack
		auto attackAnim = Animate::create(AnimationCache::getInstance()->getAnimation("leader_attack1"));
		attackAnim->setTag(Utility::ANIMATION);
		
		//making an array of actions
		Vector<FiniteTimeAction*> arrayOfActions;

		//Stop gravity + stop player->vel
		arrayOfActions.pushBack(CallFunc::create(
			[player]()
		{
			player->getPhysicsBody()->setGravityEnable(false);
			player->getPhysicsBody()->setVelocity(Vec2::ZERO);
		}
		));
		//Fake delay
		arrayOfActions.pushBack(DelayTime::create(.2f));
		//Movement itself + raycast for hits mayhaps
		arrayOfActions.pushBack(CallFunc::create(
			[player]()
		{
			bool sign = std::signbit( player->getScaleX());
			float dir = sign ? -1.f : 1.f;

			//Raycast ig
			Vec2 targetPos = player->getScene()->convertToNodeSpace(Vec2(travelDistance, 0) * dir + player->getPosition());
			//Reaycast func
			auto func = [&targetPos](PhysicsWorld& world, const PhysicsRayCastInfo& rayHit, void* userData)->bool
			{
				//If is inside a ground object, skip this point
				/*if (rayHit.shape->getCategoryBitmask() == Utility::LayerGround)
				{
					targetPos = rayHit.contact;
					return false;
				}*/

				//Return true from the callback to continue rect queries
				//Just give me wtver point
				targetPos = rayHit.contact;

				return true;
			};

			Vec2 start = player->getScene()->convertToNodeSpace(player->getPosition());
			Vec2 end = targetPos;

			player->getScene()->getPhysicsWorld()->rayCast(func, start, end, nullptr);

			targetPos = player->getParent()->convertToNodeSpace(targetPos);

			/*
			//Checks against all existing physicsboundaries if its inside any
			cocos2d::Director::getInstance()->getRunningScene()->getPhysicsWorld()->queryRect(func, Rect(targetPos.x, targetPos.y, player->getContentSize().width, player->getContentSize().height), nullptr);
			*/

			//Animation of the skill
			auto scene = player->getScene();
			auto laz = Sprite::createWithSpriteFrameName(lazer);
			laz->setCameraMask((unsigned short)CameraFlag::USER1, false);
			laz->setPosition(player->getPosition());
			//Scale accordingly
			float xScale = std::abs(targetPos.x - player->getPositionX()) / laz->getContentSize().width;
			laz->setScaleX(dir * xScale);

			//animSequence
			auto lazerAnim = Animate::create(AnimationCache::getInstance()->getAnimation("pianus_lazer"));
			lazerAnim->setTag(Utility::ANIMATION);

			laz->runAction(Sequence::createWithTwoActions(
				lazerAnim,
				CallFunc::create(
					[laz]()
			{
				laz->removeFromParentAndCleanup(true);
			}
			)));
			scene->addChild(laz);


			player->setPosition(targetPos);
		}
		));

		arrayOfActions.pushBack(DelayTime::create(.5f));
		arrayOfActions.pushBack(CallFunc::create(
			[player]()
		{
			player->skillLock = false;
			player->getPhysicsBody()->setGravityEnable(true);
		}
		));

		//Animation after attack
		arrayOfActions.pushBack(CallFunc::create(
			[player]()
		{
			player->Idle();
		}
		));

		auto skillEvent = Sequence::create(arrayOfActions);
		player->stopAllActions();
		player->runAction(skillEvent);
		player->runAction(attackAnim);
	}
		break;
	case Skills::NONE:
	default:
		break;
	}
}

