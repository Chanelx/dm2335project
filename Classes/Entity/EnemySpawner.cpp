#include "EnemySpawner.h"
#include "Utility.h"

#define MAX(a,b) a>b?a:b
#define MIN(a,b) a<b?a:b

EnemySpawner::EnemySpawner(cocos2d::Scene* scene)
{
	this->scene = scene;
}

EnemySpawner::~EnemySpawner()
{
	scene = NULL;
}

void EnemySpawner::Init(char* spawnPath, float respawnTimer, int maxEnemySpawn, std::vector<cocos2d::ValueMap> spawnPoints, cocos2d::Node* tilemap)
{
	this->respawnTimer = 0;
	this->maxRespawnTimer = respawnTimer;
	this->currentEnemySpawn = 0;
	this->maxEnemySpawn = maxEnemySpawn;
	//this->maxEnemySpawn = 1;

	//if no spawn path
	if (spawnPath == "")
		return;

	std::string path = spawnPath;
	std::string delimiter = "/";
	//check correct spawn path

	size_t pos = 0;
	while ((pos = path.find(delimiter)) != std::string::npos) {
		sceneNodePath.push_back(path.substr(0, pos));
		path.erase(0, pos + delimiter.length());
	}
	if (sceneNodePath.at(0) != "this")
		throw "Invalid File Path";

	this->spawnPoints = spawnPoints;
	this->tilemap = tilemap;
}

void EnemySpawner::Update(float dt)
{
	respawnTimer -= dt;
	if (respawnTimer <= 0.f)
	{
		respawnTimer = maxRespawnTimer;
		SpawnEnemies();
	}
}

void EnemySpawner::EnemyDied(int number)
{
	currentEnemySpawn = MAX(currentEnemySpawn - number, 0);
}

//Check for max num enemies in the scene and spawn stuff maybe
void EnemySpawner::SpawnEnemies()
{
	cocos2d::Node* spawnNode = scene;
	//Get the node to spawn the mobs in
	for (auto childs : sceneNodePath)
	{
		//Skip the first
		if (childs == "this")
			continue;
		spawnNode = spawnNode->getChildByName(childs.c_str());
	}

	while (currentEnemySpawn < maxEnemySpawn)
	{
		using namespace cocos2d;
		using namespace Utility;
		++currentEnemySpawn;

		//Choose 1 random spawn point
		int point = rand() % spawnPoints.size();

		auto enemySprite = Enemy::create();
		enemySprite->SetAnimNames("mushmummy_walk", "mushmummy_jump", "mushmummy_idle", "mushmummy_dead", "mushmummy_attack", "mushmummy_hurt");

		auto spawnPoint = spawnPoints.at(point);
		int xPos = (spawnPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
		int yPos = (spawnPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
		enemySprite->setPosition(xPos, yPos);

		//Animation
		cocos2d::Animation* anim;
		switch (rand() % 3)
		{
		case 0:
		default:
			enemySprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("mushmummy_idle1.png"));
			anim = AnimationCache::getInstance()->getAnimation("mushmummy_idle");
			break;
		case 1:
			enemySprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("mushmummy_idle1.png"));
			anim = AnimationCache::getInstance()->getAnimation("mushmummy_idle");
			break;
		case 2:
			enemySprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("mushmummy_attack1.png"));
			anim = AnimationCache::getInstance()->getAnimation("mushmummy_attack");
			break;
		}
		enemySprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		//anim = AnimationCache::getInstance()->getAnimation("gangster_die");
		//anim = AnimationCache::getInstance()->getAnimation("gangster_idle");
		//anim = AnimationCache::getInstance()->getAnimation("gangster_attack");
		enemySprite->setScaleX(-abs(enemySprite->getScaleX()));
		auto moveAnim = RepeatForever::create(Animate::create(anim));
		moveAnim->setTag(ANIMATION);
		enemySprite->runAction(moveAnim);
		StateMachine* sm = (StateMachine*)(scene->getChildByName("stateMachine"));
		sm->SetNextState(enemySprite, "StateIdle");
		//StateMachine::getInstance()->SetNextState(enemySprite, "StateIdle");

		//adding physics body
		PhysicsBody* ephysicsBody = PhysicsBody::createBox(Size(enemySprite->getContentSize().width, enemySprite->getContentSize().height), PhysicsMaterial(.1f, 0.f, 0.f));
		ephysicsBody->setCollisionBitmask(LayerGround);
		ephysicsBody->setCategoryBitmask(LayerEnemy);
		ephysicsBody->setContactTestBitmask(LayerGround | LayerPlayer);
		ephysicsBody->setRotationEnable(false);
		enemySprite->addComponent(ephysicsBody);
		enemySprite->setCameraMask((unsigned short)CameraFlag::USER1);

		//Add as child of scene
		spawnNode->addChild(enemySprite);
	}

}