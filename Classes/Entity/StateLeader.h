#ifndef _STATE_LEADER_H_
#define _STATE_LEADER_H_

#include "cocos2d.h"
#include <string>
#include "State.h"
class Enemy;

class StateIdle :public State
{

public:
	StateIdle();
	StateIdle(cocos2d::Scene * scene);
	~StateIdle();

	virtual void Enter(Enemy* enemy);
	virtual void Exit(Enemy* enemy);
	virtual void Update(float dt, Enemy* enemy);

};

class StateAggro :public State
{
public:
	StateAggro();
	StateAggro(cocos2d::Scene * scene);
	~StateAggro();

	virtual void Enter(Enemy* enemy);
	virtual void Exit(Enemy* enemy);
	virtual void Update(float dt, Enemy* enemy);

};



#endif