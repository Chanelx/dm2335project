#ifndef _STATEMACHINE_H_
#define _STATEMACHINE_H_

#include "cocos2d.h"
#include <string>
#include "State.h"
//USING_NS_CC;
//class Entity;
class Enemy;

class StateMachine : public cocos2d::Node
{
	//static StateMachine* instance;
public:
	//static StateMachine* getInstance();

	StateMachine();
	~StateMachine();

	void Update(float dt, Enemy* enemy);

	std::map<std::string, State*> stateMap;
	void AddState(std::string stateName, State* newState);
	void SetNextState(Enemy* enemy, const std::string & nextStateID);
	const std::string& GetCurrentState(Enemy* enemy);

	static StateMachine* create();
};


#endif