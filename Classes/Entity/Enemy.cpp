#include "Enemy.h"


Enemy::Enemy()
	:curState(nullptr),
	nextState(nullptr),
	stateTimer(0.0f)
{
}


Enemy::~Enemy()
{
}

void Enemy::MoveLeft()
{
}

void Enemy::MoveRight()
{
}

void Enemy::Jump()
{
}

Enemy * Enemy::create()
{
	//copied from sprite
	Enemy* enemy = new (std::nothrow) Enemy();
	if (enemy)
	{
		enemy->autorelease(); //could delete away this line if we want to carry it between scenes
		return enemy;
	}
	CC_SAFE_DELETE(enemy);
	return nullptr;
}
