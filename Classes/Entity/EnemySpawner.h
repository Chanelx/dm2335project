#ifndef _ENEMY_SPAWNER_H_
#define _ENEMY_SPAWNER_H_

//#include "Entity.h"
#include "cocos2d.h"
#include "Enemy.h"
#include "StateMachine.h"
class EnemySpawner
{
	float maxRespawnTimer;
	float respawnTimer;

	int maxEnemySpawn;
	int currentEnemySpawn;
	
	std::vector<std::string> sceneNodePath;

	cocos2d::Node* tilemap;
	std::vector<cocos2d::ValueMap> spawnPoints;

	//Make private so bobos dont call this
	void SpawnEnemies();
public:
	EnemySpawner(cocos2d::Scene* scene);
	~EnemySpawner();
	void Init(char* spawnPath, float respawnTimer, int maxEnemySpawn, std::vector<cocos2d::ValueMap> spawnPoints, cocos2d::Node* tilemap);
	void Update(float dt);

	void EnemyDied(int number);

	cocos2d::Scene* scene;
};


#endif // _ENEMY_SPAWNER_H_