#ifndef _PIANUS_ROCKS_H_
#define _PIANUS_ROCKS_H_

#include "2d/CCComponent.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

class PianusRocks : public cocos2d::Component
{
	enum CurrentState
	{
		Marker,
		Waiting,
		Falling,
		Explode,
		TOTAL
	};

	CurrentState state;
	float timeLeft;

	static const std::string rock;
	static const std::string marker;

	int fallCount;
public:

	PianusRocks();
	~PianusRocks();

	virtual void update(float delta);

	virtual bool init();
	//virtual bool init(float maxCooldown);

	

	static PianusRocks* create();
	//static PianusRocks* create(float maxCooldown);
};


#endif // _PLAYER_H_