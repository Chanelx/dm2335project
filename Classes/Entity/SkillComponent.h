#ifndef _SKILL_COMPONENT_H_
#define _SKILL_COMPONENT_H_

#include "2d/CCComponent.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

class SkillComponent : public cocos2d::Component
{
public:

	enum Skills
	{
		NothingPersonalKid,
		NONE
	};

	SkillComponent();
	~SkillComponent();

	float maxCooldown;
	float cooldown;
	bool onCooldown;

	Skills currentSkill;

	virtual void update(float delta);

	virtual bool init();
	virtual bool init(float maxCooldown);

	void SetSkill(Skills skill);

	static const std::string lazer;

	static SkillComponent* create();
	static SkillComponent* create(float maxCooldown);

	void ActivateSkill(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type);
};


#endif // _PLAYER_H_