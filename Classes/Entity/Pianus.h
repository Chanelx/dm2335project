#ifndef _PIANUS_H_
#define _PIANUS_H_
#include "cocos2d.h"

class Pianus : public cocos2d::Component
{
protected:
	std::string idleAnimName;
	//Idk how many skills and anims we gona have
	std::string skillAnimName1;
	std::string skillAnimName2;

public:
	enum PianusState
	{
		IDLE,
		SKILL,
		NONE
	};

	Pianus();
	~Pianus();

	float cooldown;
	float maxCooldown;
	PianusState state;
	PianusState prevState;	//Too retarded to think of another method

	void SetAnimNames(std::string idle);

	virtual void update(float delta);
	void ActivateSkill();

	virtual bool init();
	virtual bool init(float cooldown);
	static Pianus* create();
	static Pianus* create(float cooldown);
};


#endif