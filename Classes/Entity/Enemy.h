#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "Entity.h"
#include "StateMachine.h"
#include "State.h"

class Enemy : public Entity
{
public:
	Enemy();
	~Enemy();
	virtual	void MoveLeft();
	virtual	void MoveRight();
	virtual	void Jump();

	State* curState;
	State* nextState;

	static Enemy* create();

	float stateTimer;

};


#endif