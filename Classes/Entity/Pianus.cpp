#include "Pianus.h"
#include "Utility.h"
#include <vector>
#include "PianusRocks.h"

Pianus::Pianus()
{
}


Pianus::~Pianus()
{
}

void Pianus::SetAnimNames(std::string idle)
{
	this->idleAnimName = idle;
}

void Pianus::update(float delta)
{
	Component::update(delta);

	//If in idle state
	if (state == IDLE)
	{
		//Animation
		if (prevState != state)
		{
			using namespace cocos2d;
			prevState = state;
			//Play anim
			Sprite* pianusSprite = (Sprite*)(this->getOwner());
			pianusSprite->setScaleX(-abs(pianusSprite->getScaleX()));

			Animation* anim;
			anim = AnimationCache::getInstance()->getAnimation(idleAnimName);
			auto moveAnim = RepeatForever::create(Animate::create(anim));
			moveAnim->setTag(Utility::ANIMATION);

			pianusSprite->stopActionByTag(Utility::ANIMATION);
			pianusSprite->runAction(moveAnim);
		}

		cooldown -= delta;
		if (cooldown <= 0.f)
		{
			cooldown = maxCooldown;

			//Activate skill
			ActivateSkill();
		}
	}
}

void Pianus::ActivateSkill()
{
	int i = 0;
	cocos2d::log("Activating pianus skill: %i", i);

using namespace cocos2d;
	auto attackAnim = Animate::create(AnimationCache::getInstance()->getAnimation("pianus_damage"));
	attackAnim->setTag(Utility::ANIMATION);

	auto sprite = (Sprite*) getOwner();
	//making an array of actions
	Vector<FiniteTimeAction*> arrayOfActions;

	//Do something
	arrayOfActions.pushBack(CallFunc::create(
		[this, sprite]()
		{
			this->state = SKILL;
			this->prevState = SKILL;

			//Get top of sprite
			auto size = sprite->getContentSize();
			//1 - 6
			int numberOfRoks = cocos2d::random() % 6 + 1;
			//std::vector<Vec2> positions;
			//Get direction of sprite facing, pianus positive scale is left
			bool bit = signbit(sprite->getScaleX());
			int dir = bit ? 1 : -1;
			for (int i = 0; i < numberOfRoks; i++)
			{
				Vec2 pos = Vec2(dir * (size.width + 50 + i * 180) + sprite->getPositionX(), size.height * 1.5 + sprite->getPositionY());
				auto testSprite = Sprite::createWithSpriteFrameName("leader_dead_1.png");
				if (testSprite)
				{
					testSprite->setPosition(pos);
					testSprite->setCameraMask((unsigned short)CameraFlag::USER1, false);
					auto comp = PianusRocks::create();
					testSprite->addComponent(comp);
					sprite->getParent()->addChild(testSprite);
				}
			}
		}
	));
	//Fake delay
	arrayOfActions.pushBack(DelayTime::create(1.5f));
	arrayOfActions.pushBack(CallFunc::create(
		[this]()
		{
			this->state = IDLE;
		}
	));

	auto skillEvent = Sequence::create(arrayOfActions);
	sprite->stopAllActions();
	sprite->runAction(skillEvent);
	sprite->runAction(attackAnim);
}

bool Pianus::init()
{
	cooldown = 1;
	maxCooldown = 1;
	state = IDLE;
	prevState = NONE;
	return true;
}

bool Pianus::init(float cooldown)
{
	this->cooldown = cooldown;
	this->maxCooldown = cooldown;
	state = IDLE;
	prevState = NONE;
	return true;
}

Pianus * Pianus::create()
{
	//copied from sprite
	Pianus* pianus = new (std::nothrow) Pianus();
	if (pianus && pianus->init())
	{
		pianus->autorelease(); //could delete away this line if we want to carry it between scenes
		return pianus;
	}
	CC_SAFE_DELETE(pianus);
	return nullptr;
}

Pianus * Pianus::create(float cooldown)
{
	//copied from sprite
	Pianus* pianus = new (std::nothrow) Pianus();
	if (pianus && pianus->init(cooldown))
	{
		pianus->autorelease(); //could delete away this line if we want to carry it between scenes
		return pianus;
	}
	CC_SAFE_DELETE(pianus);
	return nullptr;
}
