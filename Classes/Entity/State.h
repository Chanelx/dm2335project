#ifndef _STATE_H_
#define _STATE_H_

#include "cocos2d.h"
#include <string>
#include "Player.h"

class Enemy;

class State
{
protected:
	std::string name;
	float distanceFromPlayer;
//	Player* player;
public:
	State();
	//State(Player* player);
	virtual ~State();

	std::string GetName();
//	void SetPlayer(Player* player);

	virtual void Enter(Enemy* enemy) = 0;
	virtual void Exit(Enemy* enemy) = 0;
	virtual void Update(float dt, Enemy* enemy) = 0;

	cocos2d::Scene* scene;
};


#endif