#include "Enemy.h"
#include "StateLeader.h"
#include "Utility.h"
//STATE IDLE *********
StateIdle::StateIdle()
{
	//name = "StateIdle";
}

StateIdle::StateIdle(cocos2d::Scene* scene)
{
	this->scene = scene;
	//name = "StateIdle";
}


StateIdle::~StateIdle()
{
}

void StateIdle::Enter(Enemy* enemy)
{
}

void StateIdle::Exit(Enemy* enemy)
{
}

void StateIdle::Update(float dt, Enemy* enemy)
{
	auto player = scene->getChildByName("spriteNode")->getChildByName("player");
	if (player)
	{
		distanceFromPlayer = enemy->getPosition().distanceSquared(player->getPosition());
		if (distanceFromPlayer < 5 * 5 * player->getContentSize().width * player->getContentSize().width) //change to aggro state when 5 "player widths" away from player
		{
			StateMachine* sm = (StateMachine*)(scene->getChildByName("stateMachine"));
			sm->SetNextState(enemy, "StateAggro");
			//StateMachine::getInstance()->SetNextState(enemy, "StateAggro");
		}
	}

	int direction = rand() % 3 - 1;
	enemy->getPhysicsBody()->setVelocity(cocos2d::Vec2(direction * 50, 0));
	if (direction != 0)
	{
		enemy->setScaleX(direction);
	}

	switch (direction)
	{
	case -1:
	case 1:
	{
		enemy->stopActionByTag(Utility::ANIMATION);
		cocos2d::Animation* anim = cocos2d::AnimationCache::getInstance()->getAnimation("mushmummy_walk");
		auto moveAnim = cocos2d::RepeatForever::create(cocos2d::Animate::create(anim));
		moveAnim->setTag(Utility::ANIMATION);
		enemy->runAction(moveAnim);
	}
	break;
	case 0:
	default:
	{
		enemy->stopActionByTag(Utility::ANIMATION);
		cocos2d::Animation* anim = cocos2d::AnimationCache::getInstance()->getAnimation("mushmummy_idle");
		auto moveAnim = cocos2d::RepeatForever::create(cocos2d::Animate::create(anim));
		moveAnim->setTag(Utility::ANIMATION);
		enemy->runAction(moveAnim);
	}
	break;
	}

}

//STATE AGGRO *********
StateAggro::StateAggro()
{
	//name = "StateAggro";
}

StateAggro::StateAggro(cocos2d::Scene* scene)
{
	this->scene = scene;
	//name = "StateAggro";
}

StateAggro::~StateAggro()
{
}

void StateAggro::Enter(Enemy* enemy)
{
}

void StateAggro::Exit(Enemy* enemy)
{
}

void StateAggro::Update(float dt, Enemy* enemy)
{
	auto player = scene->getChildByName("spriteNode")->getChildByName("player");
	if (player)
	{
		//aggro actions
		int direction =0;
		if (player->getPositionX() > enemy->getPositionX()) //if player is right of enemy, move enemy to player
		{
			direction = 1;
			enemy->setScaleX(direction);

		}
		else if (player->getPositionX() < enemy->getPositionX())//if player is left of enemy, move enemy to player
		{
			direction = -1;
			enemy->setScaleX(direction);
		}
		//if player is rightabove enemy, default 0
		enemy->getPhysicsBody()->setVelocity(cocos2d::Vec2(direction * 75, 0));
		enemy->stopActionByTag(Utility::ANIMATION);
		cocos2d::Animation* anim = cocos2d::AnimationCache::getInstance()->getAnimation("mushmummy_attack");
		auto moveAnim = cocos2d::RepeatForever::create(cocos2d::Animate::create(anim));
		moveAnim->setTag(Utility::ANIMATION);
		enemy->runAction(moveAnim);

		distanceFromPlayer = enemy->getPosition().distanceSquared(player->getPosition());

		if (distanceFromPlayer > 6 * 6 * player->getContentSize().width * player->getContentSize().width)//change to idle state when 6 "player widths" away from player
		{
			StateMachine* sm = (StateMachine*)(scene->getChildByName("stateMachine"));
			sm->SetNextState(enemy, "StateIdle");
			//StateMachine::getInstance()->SetNextState(enemy, "StateIdle");
		}

	}
}
