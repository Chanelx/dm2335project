#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Entity.h"

class Player : public Entity
{
public:

	Player();
	~Player();

	virtual	void MoveLeft();
	virtual	void MoveRight();
	virtual	void Jump();

	virtual	void MoveUp();
	virtual	void MoveDown();

	void FlashJump();
	void UnhangRope();

	bool skillLock = false;

	bool onPortal = false;
	bool onRope = false;
	bool touchingRope = false;

	bool flashJump = true;
	bool flashJumpUnlocked = true;

	//Some random error number
	float ropeXPos = INT_MIN;

	void UpdateOnPortal(bool onPortal);

	static Player* create();

	int currency;
};


#endif // _PLAYER_H_