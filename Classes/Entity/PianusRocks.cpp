#include "PianusRocks.h"
#include "Player.h"
#include "Utility.h"

const std::string PianusRocks::rock = "pianus_attack1.png";
const std::string PianusRocks::marker = "pianus_cast1.png";
//const std::string PianusRocks::rock = "leader_dead_1.png";
//const std::string PianusRocks::marker = "mushmummy_dead4.png";

PianusRocks::PianusRocks()
{
	Component::Component();
}


PianusRocks::~PianusRocks()
{
}


void PianusRocks::update(float delta)
{
	Component::update(delta);

	auto owner = (cocos2d::Sprite*)getOwner();

	timeLeft -= delta;
	if (state == Marker)
	{
using namespace cocos2d;

		owner->setContentSize(Size(0, 0));
		Vec2 pointToSpawn;
		auto func = [&pointToSpawn](PhysicsWorld& world,
			const PhysicsRayCastInfo& info, void* data)->bool
		{
			//Find a rock to collide
			if (info.shape->getCategoryBitmask() != Utility::LayerGround)
				return false;
			pointToSpawn = info.contact + Vec2(0,100);
			return true;
		};
		Vec2 worldOwnerPos = owner->getScene()->convertToNodeSpace(owner->getPosition());
		owner->getScene()->getPhysicsWorld()->rayCast(func, worldOwnerPos, worldOwnerPos + Vec2(0, -100000), nullptr);

		using namespace cocos2d;
		auto markAnim = Animate::create(AnimationCache::getInstance()->getAnimation("pianus_cast"));
		markAnim->setTag(Utility::ANIMATION);

		auto marker = Sprite::createWithSpriteFrameName(PianusRocks::marker);
		marker->setCameraMask((unsigned short)CameraFlag::USER1, false);
		marker->setName("Marker");
		owner->addChild(marker);
		marker->setPosition(owner->convertToNodeSpace(pointToSpawn));
		marker->runAction(markAnim);
		marker->setScale(2);

		state = Waiting;
	}
	else if (state == Waiting)
	{
		if (timeLeft <= 1.5f)
		{
			state = Falling;
			using namespace cocos2d;
			auto attackAnim = Animate::create(AnimationCache::getInstance()->getAnimation("pianus_attack"));
			attackAnim->setTag(Utility::ANIMATION);

		//	owner->setSpriteFrame(cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(rock));
			owner->runAction(attackAnim);
			owner->setScale(2);

			auto marker = owner->getChildByName("Marker");
			owner->setPosition(owner->getPosition() + marker->getPosition() + cocos2d::Vec2(0, 300));
			marker->setContentSize(cocos2d::Size(0, 0));
		}
	}
	
	else if (state == Falling)
	{
		--fallCount;
		owner->setPosition(owner->getPosition() - cocos2d::Vec2(0, 30));
		if (fallCount <= 0)
			state = Explode;
	}
	
	else if (state == Explode)
	{
		state = TOTAL;
using namespace cocos2d;
		//Play animation magic
		Vector<FiniteTimeAction*> arrayOfActions;
		//Put in animation here, using time as placeholder
		arrayOfActions.pushBack(DelayTime::create(.65f));
		arrayOfActions.pushBack(CallFunc::create(
			[owner]()
		{
			owner->removeFromParentAndCleanup(true);
		}
		));
		auto skillEvent = Sequence::create(arrayOfActions);
		owner->runAction(skillEvent);
/*
		auto phyBody = PhysicsBody::createBox(owner->getContentSize(), PHYSICSBODY_MATERIAL_DEFAULT, owner->getContentSize() * -.5f);
		phyBody->setCategoryBitmask(Utility::LayerTrigger);
		phyBody->setContactTestBitmask(Utility::LayerPlayer);
		phyBody->setDynamic(false);*/

	//	owner->setName("Rock");
	//	owner->setPhysicsBody(phyBody);
	}
}

bool PianusRocks::init()
{
	state = CurrentState::Marker;
	timeLeft = 3.f;
	fallCount = 10; //how many counts to fall
	return true;
}
//
//bool PianusRocks::init(float time)
//{
//	return true;
//}

PianusRocks * PianusRocks::create()
{
	//copied from Component
	PianusRocks * ret = new (std::nothrow) PianusRocks();

	if (ret && ret->init())
	{
		ret->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(ret);
	}

	return ret;
}
/*
PianusRocks * PianusRocks::create(float maxCooldown)
{
	//copied from Component
	PianusRocks * ret = new (std::nothrow) PianusRocks();

	if (ret && ret->init(maxCooldown))
	{
		ret->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(ret);
	}

	return ret;
}*/