/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __EMPTYSCENE_SCENE_H__
#define __EMPTYSCENE_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "SceneBase.h"

class EmptyScene : public SceneBase
{
	cocos2d::Vec2 mousePos;
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(EmptyScene);

	//void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void ButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type);

	virtual	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) ;
	virtual	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	virtual	void onMouseMove(cocos2d::Event* event) ;
	virtual	void onMouseUp(cocos2d::Event* event) ;
	virtual	void onMouseDown(cocos2d::Event* event) ;
	virtual	void onMouseScroll(cocos2d::Event* event) ;

	//Button responses
	virtual	void LeftButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type) ;
	virtual	void RightButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type) ;
	virtual	void JumpButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type) ;
	virtual	void InteractButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type) ;
	virtual	void BackButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type) ;

	virtual	bool onContactBegin(cocos2d::PhysicsContact& begin) ;
	virtual	bool onContactSeperate(cocos2d::PhysicsContact& begin) ;

	enum ActionTags
	{
		MOVEMENT = 1,
		ANIMATION = 2
	};


	//cocos2d::Vector<cocos2d::SpriteFrame*> sprites;
};
#endif // __EmptyScene_SCENE_H__
