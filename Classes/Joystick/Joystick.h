#ifndef _JOYSTICK_H_
#define _JOYSTICK_H_

#include "cocos2d.h"
#include <string>

class Joystick : public cocos2d::Node
{
public:
	int currentTouchID;
	float radius;

	//To check if its active
	bool active;

	cocos2d::Vec2 currentDirection;

	static Joystick* create();

	virtual void release();
	void SetRadius(float radius);
	
	void SetJoystickPos(cocos2d::Vec2 pos);
	
	bool joystickTouchBegan(cocos2d::Touch* touch, cocos2d::Event* _event);
	bool joystickTouchMove(cocos2d::Touch* touch, cocos2d::Event* _event);
	bool joystickTouchEnd(cocos2d::Touch* touch, cocos2d::Event* _event);
};

#endif // _JOYSTICK_H_
