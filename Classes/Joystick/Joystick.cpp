#include "Joystick.h"

void Joystick::release()
{
}
void Joystick::SetRadius(float radius)
{
	this->radius = radius;
}

void Joystick::SetJoystickPos(cocos2d::Vec2 pos)
{
	using namespace cocos2d;

	active = pos != Vec2::ZERO;

	auto innerCircle = (Sprite*)Node::getChildByName("Inner Circle");
	//Do something
	if (innerCircle)
	{
		innerCircle->setPosition(pos);
		currentDirection = pos;
	}
}

Joystick* Joystick::create()
{
	//copied from sprite
	Joystick* joystick = new (std::nothrow) Joystick();
	if (joystick)
	{
		joystick->autorelease(); //could delete away this line if we want to carry it between scenes

		using namespace cocos2d;

		joystick->radius = 0.f;
		joystick->active = false;
		joystick->currentTouchID = -1;

		return joystick;
	}
	CC_SAFE_DELETE(joystick);
	return nullptr;
}


bool Joystick::joystickTouchBegan(cocos2d::Touch * touch, cocos2d::Event * _event)
{
	using namespace cocos2d;
	Vec2 test = convertTouchToNodeSpace(touch);

	//If already active OR distance wrong, do nothing
	if(active || test.getLengthSq() > radius * radius)
		return false;

	currentTouchID = touch->getID();

	SetJoystickPos(test);

	
	return true;
}

bool Joystick::joystickTouchMove(cocos2d::Touch * touch, cocos2d::Event * _event)
{
	using namespace cocos2d;

	//If not related, do nothing
	if (!active || touch->getID() != currentTouchID)
		return false;

	Vec2 test = convertTouchToNodeSpace(touch);
	test = test.lengthSquared() > radius * radius ? test.getNormalized() * radius : test;

	SetJoystickPos(test);

	return true;
}

bool Joystick::joystickTouchEnd(cocos2d::Touch * touch, cocos2d::Event * _event)
{
	using namespace cocos2d;

	//If not related, do nothing
	if (!active || touch->getID() != currentTouchID)
		return false;

	currentTouchID = -1;
	//Do something
	SetJoystickPos(Vec2::ZERO);
	return true;
}