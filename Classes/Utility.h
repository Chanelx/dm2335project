#pragma once
namespace Utility
{
	enum LayerEnums
	{
		LayerTrigger = 0x08,
		LayerPlayer = 0x01,
		LayerGround = 0x02,
		LayerEnemy = 0x04,
		//Layer4 = 0x08,
		Layer5 = 0x16,
		Layer6 = 0x32,
		Layer7 = 0x64,
		Layer8 = 0x128,
		Layer9 = 0x256,
	};
	enum ActionTags
	{
		MOVEMENT = 1,
		ANIMATION = 2,
		CAMERA = 3
	};

	int wrap(int i, int min, int max);
	float clamp(float i, float min, float max);
};

