/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "EmptyScene.h"
#include "HelloWorldScene2.h"
#include "Utility.h"

#include "Entity/SkillComponent.h"
#include "Entity/Pianus.h"

using namespace Utility;

USING_NS_CC;
//kermit suicide
Scene* HelloWorld::createScene()
{
	//	auto scene = Scene::create();
	auto scene = Scene::createWithPhysics();
	//scene->getPhysicsWorld()->setGravity(Vec2(0, 0));
	scene->getPhysicsWorld()->setGravity(Vec2(0, -980.f));
	auto layer = HelloWorld::create();
	scene->addChild(layer, 0, 999);
	scene->getPhysicsWorld()->setDebugDrawMask(0xfffff);
	//return layer;
	return scene;

	//no physics
	//auto layer = HelloWorld::create();
	//return layer;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	//printf("Error while loading: %s\n", filename);
	//printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	////////////////////////////////
	//// 1. super init first
	//if (!Scene::init())
	//{
	//	return false;
	//}

	//this->scheduleUpdate();

	//worldCamera = Camera::create();
	////worldCamera = getDefaultCamera();
	//worldCamera->setCameraFlag(CameraFlag::USER1);
	//addChild(worldCamera);

	SceneBase::init();

	//isGrounded = false;

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//this->setPhysics3DDebugCamera(worldCamera);
	//auto audioSource = CocosDenshion::SimpleAudioEngine::getInstance();
	if (!audioSource->isBackgroundMusicPlaying())
	{
		audioSource->playBackgroundMusic("Audio/HenesysHuntingGround.mp3");
	}
	/////////////////////////////
//	getDefaultCamera()->setDepth(1);
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.
	/*
	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create(
										   "CloseNormal.png",
										   "CloseSelected.png",
										   CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

	if (closeItem == nullptr ||
		closeItem->getContentSize().width <= 0 ||
		closeItem->getContentSize().height <= 0)
	{
		problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
	}
	else
	{
		float x = origin.x + visibleSize.width - closeItem->getContentSize().width/2;
		float y = origin.y + closeItem->getContentSize().height/2;
		closeItem->setPosition(Vec2(x,y));
	}

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);
	*/
	/////////////////////////////
	// 3. add your codes below...

	// add a label shows "Hello World"
	// create and initialize a label
	/*
	auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
	if (label == nullptr)
	{
		problemLoading("'fonts/Marker Felt.ttf'");
	}
	else
	{
		// position the label on the center of the screen
		label->setPosition(Vec2(origin.x + visibleSize.width/2,
								origin.y + visibleSize.height - label->getContentSize().height));

		// add the label as a child to this layer
		this->addChild(label, 1);
	}

	// add "HelloWorld" splash screen"
	auto sprite = Sprite::create("HelloWorld.png");
	if (sprite == nullptr)
	{
		problemLoading("'HelloWorld.png'");
	}
	else
	{
		// position the sprite on the center of the screen
		sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

		// add the sprite as a child to this layer
		this->addChild(sprite, 0);
	}
	*/


	Size playingSize = Size(visibleSize.width, visibleSize.height - (visibleSize.height / 8));

	//Node Container
	//auto nodeItems = Node::create();
	//nodeItems->setName("nodeItems");
	//this->addChild(nodeItems, 1);

	//StateMachine
	//sm = new StateMachine();
	//sm->AddState("StateIdle", new StateIdle());
	//sm->AddState("StateAggro", new StateAggro());

	//init tile map
	auto tilemap = InitializeTilemap("Tilemaps/HuntingGround.tmx", "Tilemap", Vec2(0, playingSize.height * 0.05f), 2);
	tilemap->setName("Tilemap");
	this->addChild(tilemap);

	//background 
	//auto paraBG = ParallaxNode::create();
	auto paraSprite = Sprite::create("henesysBG.png");
	paraSprite->setName("Background");
	paraSprite->setScale(5);
	paraSprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	paraSprite->setPosition(Vec2::ZERO);
	paraSprite->setZOrder(-10);
	//paraBG->addChild(paraSprite, 1, Vec2(.9f,.9f), Vec2::ZERO);
	this->addChild(paraSprite);


	////Sprite Sheet
	//SpriteBatchNode* spriteBatch = SpriteBatchNode::create("sprite.png");
	//SpriteFrameCache* cache = SpriteFrameCache::getInstance();
	//cache->addSpriteFramesWithFile("sprite.plist");
	//AnimationCache::getInstance()->addAnimationsWithFile("sprite_ani.plist");

	////making mushmummy test
	//SpriteBatchNode* mushBatch = SpriteBatchNode::create("mushmummySprite.png");
	//cache->addSpriteFramesWithFile("mushmummy.plist");
	//AnimationCache::getInstance()->addAnimationsWithFile("mushmummy_ani.plist");

	////making gangster test
	//SpriteBatchNode* gangBatch = SpriteBatchNode::create("gangsterSprite.png");
	//cache->addSpriteFramesWithFile("gangster.plist");
	//AnimationCache::getInstance()->addAnimationsWithFile("gangster_ani.plist");

	SpriteFrameCache* cache = SpriteFrameCache::getInstance();
	//getting a reference to the objects layer in tilemap
	TMXObjectGroup* objectGroup = tilemap->objectGroupNamed("Objects");

	//Sprites
	auto spriteNode = Node::create();
	spriteNode->setName("spriteNode");
	this->addChild(spriteNode, 1);

	auto enemyNode = Node::create();
	enemyNode->setName("enemyNode");
	this->addChild(enemyNode, 1);

	//////mainsprite
	//auto mainSprite = Sprite::create();
	//mainSprite->setName("mainSprite");
	//mainSprite->setSpriteFrame(cache->getSpriteFrameByName("mushmummy_idle1.png"));
	//mainSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	//////getting spawnpoint from the tilemap, pos in tilemap + tilemap's pos in prog whole thing multiply by scale
	//ValueMap spawnPoint = objectGroup->objectNamed("SpawnPoint");
	//int xPos = (spawnPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
	//int yPos = (spawnPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
	//mainSprite->setPosition(xPos, yPos);

	//Animation* anim = AnimationCache::getInstance()->getAnimation("mushmummy_idle");
	//mainSprite->setScaleX(-abs(mainSprite->getScaleX()));
	//auto moveAnim = RepeatForever::create(Animate::create(anim));
	//moveAnim->setTag(ANIMATION);
	//mainSprite->runAction(moveAnim);

	//////adding physics body
	////PhysicsBody* physicsBody = PhysicsBody::createBox(mainSprite->getContentSize(), PhysicsMaterial(.1f, 0.f, 1.f));
	////physicsBody->setCategoryBitmask(LayerPlayer);
	////physicsBody->setCollisionBitmask(LayerGround);
	////physicsBody->setContactTestBitmask(LayerGround | LayerEnemy | LayerTrigger);
	////physicsBody->setRotationEnable(false);
	////mainSprite->addComponent(physicsBody);

	//spriteNode->addChild(mainSprite, 1);

	auto player = Player::create();
	player->setName("player");
	player->setSpriteFrame(cache->getSpriteFrameByName("leader_idle_1.png"));
	player->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);

	ValueMap spawnPoint = objectGroup->objectNamed("SpawnPoint");
	int xPos = (spawnPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
	int yPos = (spawnPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
	player->setPosition(xPos, yPos);

	Animation* anim = AnimationCache::getInstance()->getAnimation("leader_idle");
	player->setScaleX(-abs(player->getScaleX()));
	auto moveAnim = RepeatForever::create(Animate::create(anim));
	moveAnim->setTag(ANIMATION);
	player->runAction(moveAnim);

	//adding physics body
	PhysicsBody* physicsBody = PhysicsBody::createBox(player->getContentSize(), PhysicsMaterial(1111.1f, 0.f, 1.f));
	physicsBody->setCategoryBitmask(LayerPlayer);
	physicsBody->setCollisionBitmask(LayerGround);
	physicsBody->setContactTestBitmask(LayerGround | LayerEnemy | LayerTrigger);
	physicsBody->setRotationEnable(false);
	physicsBody->setName("physicsBody");
	player->addComponent(physicsBody);
	float mass = physicsBody->getMass();
	//physicsBody->setMass(PHYSICS_INFINITY);

	player->SetAnimNames("leader_move", "leader_jump", "leader_idle", "leader_dead", "leader_attack1", "leader_hit");
	//player->Idle();

	spriteNode->addChild(player, 1);
	auto idleState = new StateIdle();
	idleState->scene = this;
	auto aggroState = new StateAggro();
	aggroState->scene = this;

	auto stateMachine = StateMachine::create();
	stateMachine->AddState("StateIdle", idleState);
	stateMachine->AddState("StateAggro", aggroState);
	stateMachine->setName("stateMachine");
	this->addChild(stateMachine);
	//StateMachine::getInstance()->AddState("StateIdle", idleState);

	//StateMachine::getInstance()->AddState("StateAggro", new StateAggro(player));

	////Test enemy
	//auto enemy = Sprite::create();
	//enemy->setName("enemySprite");
	//enemy->setSpriteFrame(cache->getSpriteFrameByName("gangster_idle1.png"));
	//enemy->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	//enemy->setPosition(300, playingSize.height);

	//Animation* eanim = AnimationCache::getInstance()->getAnimation("gangster_idle");
	//enemy->setScaleX(-abs(mainSprite->getScaleX()));
	//enemy->stopActionByTag(ANIMATION);
	//moveAnim = RepeatForever::create(Animate::create(eanim));
	//moveAnim->setTag(ANIMATION);
	//enemy->runAction(moveAnim);


	////adding physics body
	//PhysicsBody* ephysicsBody = PhysicsBody::createBox(Size(enemy->getContentSize().width, enemy->getContentSize().height), PhysicsMaterial(.1f, 0.f, 0.f));
	//ephysicsBody->setCollisionBitmask(LayerGround);
	//ephysicsBody->setCategoryBitmask(LayerEnemy);
	//ephysicsBody->setContactTestBitmask(LayerGround | LayerPlayer);
	//ephysicsBody->setRotationEnable(false);
	//enemy->addComponent(ephysicsBody);

	//spriteNode->addChild(enemy, 1);


	//enemySpawner->Init("this/spriteNode/", 1, 1);
	InitializeObjects();

	auto pianusSprite = Sprite::create();
	pianusSprite->setPosition(player->getPosition() + Vec2(0, -125));
	pianusSprite->setSpriteFrame(cache->getSpriteFrameByName("pianus_idle1.png"));
	pianusSprite->setScale(2, 2);
	spriteNode->addChild(pianusSprite, 1);

	auto pianusComp = Pianus::create(5.f);
	pianusComp->SetAnimNames("pianus_idle");
	pianusSprite->addComponent(pianusComp);

	//auto portalSprite = Sprite::create("Portal.png");
	//portalSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	//portalSprite->setName("portal");
	///*
	//ValueMap portalPoint = objectGroup->objectNamed("SpawnPoint");
	//xPos = (portalPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
	//yPos = (portalPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
	//portalSprite->setPosition(xPos, yPos);
	//*/
	//ValueMap portalPoint = objectGroup->objectNamed("Portal");
	//xPos = (portalPoint.at("x").asInt() + tilemap->getPositionX()) * tilemap->getScale();
	//yPos = (portalPoint.at("y").asInt() + tilemap->getPositionY()) * tilemap->getScale();
	//portalSprite->setPosition(xPos, yPos);
	//portalSprite->setScale(2);

	//anim = AnimationCache::getInstance()->getAnimation("portal_anim");
	//moveAnim = RepeatForever::create(Animate::create(anim));
	//moveAnim->setTag(ANIMATION);
	//portalSprite->runAction(moveAnim);

	////portalSprite->setPosition(100, playingSize.height * 0.75f);
	////adding physics body
	//auto portalBody = PhysicsBody::createBox(Size(portalSprite->getContentSize().width, portalSprite->getContentSize().height), PhysicsMaterial(0.1f, 0.5f, 0.0f));

	////portalBody->setCollisionBitmask(LayerGround);
	//portalBody->setCategoryBitmask(LayerTrigger);
	//portalBody->setContactTestBitmask(LayerPlayer);
	//portalBody->setRotationEnable(false);
	//portalBody->setDynamic(false);
	//portalSprite->addComponent(portalBody);

	//spriteNode->addChild(portalSprite, 1);

	//spawning enemies into the scene
	//SpawnEnemies();

	//for (auto enemy : this->getChildByName("enemyNode")->getChildren())
	//{
	//	StateMachine::getInstance()->SetNextState((Enemy*)enemy, "StateIdle");
	//}


	//Events
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegin, this);
	contactListener->onContactSeparate = CC_CALLBACK_1(HelloWorld::onContactSeperate, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(HelloWorld::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(HelloWorld::onKeyReleased, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);


	//--------------------------------Joystick
	Sprite* outerCircle = Sprite::create("joystick_b.png");
	outerCircle->setScale(0.3f, 0.3f);
	outerCircle->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	outerCircle->setName("Outer Circle");


	auto innerCircle = Sprite::create("joystick_d.png");
	innerCircle->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	innerCircle->setScale(0.3f, 0.3f);
	innerCircle->setName("Inner Circle");

	//Creating a joystick
	Joystick* joystick = (Joystick*)(Joystick::create());
	this->addChild(joystick, 1);
	joystick->setPosition(Vec2(playingSize.width * .15f, playingSize.height * .25f));
	joystick->addChild(outerCircle, 1);
	outerCircle->setPosition(0, 0);
	joystick->addChild(innerCircle, 1);
	innerCircle->setPosition(0, 0);

	joystick->setName("Joystick");
	//Radius of the joystick -> Half of pixel dimension of the sprite used at the back or someth
	joystick->SetRadius(64.f);

	//Touch Event
	auto joystickListener = EventListenerTouchOneByOne::create();
	//Swallow touches so it the callback dosent goto other places
	joystickListener->setSwallowTouches(true);
	//Put callbacks as joystick functions
	joystickListener->onTouchBegan = CC_CALLBACK_2(Joystick::joystickTouchBegan, joystick);
	/*[&](Touch* touch, Event* events)
	{
		Vec2 test = convertToNodeSpace(touch->getLocationInView());
		if(joystick)
		log("touched: %f", test.x);
		log("touch id = %i", touch->getID());
		return true;
	};*/
	joystickListener->onTouchMoved = CC_CALLBACK_2(Joystick::joystickTouchMove, joystick);
	/*[](Touch* touch, Event* event)
	{
		log("touch id = %i", touch->getID());
		return true;
	};*/
	joystickListener->onTouchEnded = CC_CALLBACK_2(Joystick::joystickTouchEnd, joystick);
	/*[](Touch* touch, Event* event)
	{
		log("ended");
		return true;
	};*/

	_eventDispatcher->addEventListenerWithSceneGraphPriority(joystickListener, joystick);

	//-------------------------------Create buttons
	/*
	//Moving Left Button
	auto moveLeftButton = ui::Button::create("LeftButtonUp.png", "LeftButtonDown.png", "LeftButtonDown.png");
	moveLeftButton->setPosition(Vec2(playingSize.width * 0.1, (playingSize.height * 0.2f)));
	moveLeftButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::LeftButtonResponse, this));

	//Moving Right Button
	auto moveRightButton = ui::Button::create("RightButtonUp.png", "RightButtonDown.png", "RightButtonDown.png");
	moveRightButton->setPosition(Vec2(playingSize.width * 0.3, (playingSize.height * 0.2f)));
	moveRightButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::RightButtonResponse, this));
	*/
	//Jumping Button
	auto jumpButton = ui::Button::create("flashjumpSkill.png", "flashjumpSkillDown.png");
	jumpButton->setPosition(Vec2(playingSize.width * 0.85, (playingSize.height * 0.2f)));
	jumpButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::JumpButtonResponse, this));

	//Interact Button
	auto interactButton = ui::Button::create("UpButtonUp.png", "UpButtonDown.png");
	interactButton->setPosition(Vec2(playingSize.width * 0.9, (playingSize.height * 0.3f)));
	interactButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::InteractButtonResponse, this));
	interactButton->setScale(.5f);

	//Skill Button
	auto skillButton = ui::Button::create("blankSkill.png", "blankSkillDown.png");
	skillButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
	skillButton->setName("Skill Button");
	skillButton->setPosition(Vec2(playingSize.width * 0.75, (playingSize.height * 0.25f)));
	auto skillCom = SkillComponent::create();
	skillCom->setName("Skill Component");
	//skillCom->SetSkill(SkillComponent::Skills::NothingPersonalKid);
	skillButton->addComponent(skillCom);

	skillButton->addTouchEventListener(CC_CALLBACK_2(SkillComponent::ActivateSkill, skillCom));
	skillButton->setScale(.5f);

	skillCom->SetSkill(SkillComponent::Skills::NONE);//Default for now

	//auto skillIcon = Sprite::create();
	//skillButton->addChild(skillIcon, 1);
	//skillIcon->setScale(1.f, 1.f);
	//skillIcon->setName("Skill Icon");
	//skillIcon->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	//skillIcon->setPosition(skillButton->getContentSize() * .5f);


	Node* menuNode = Node::create();
	menuNode->setName("menuNode");

	ui::ImageView* iv = ui::ImageView::create("Shop/shopScreen.png");
	iv->setName("shopScreen");
	iv->setPosition(Vec2(playingSize.width * 0.5f, (playingSize.height * 0.5f)));
	iv->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	menuNode->addChild(iv);

	ui::ImageView* itemImage = ui::ImageView::create("Shop/dankuusen.png");
	itemImage->setPosition(Vec2(iv->getContentSize().width * 0.05f, iv->getContentSize().height * 0.5f));
	itemImage->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	iv->addChild(itemImage);
	ui::ImageView* itemImage2 = ui::ImageView::create("Shop/flashjump.png");
	itemImage2->setPosition(Vec2(iv->getContentSize().width * 0.05f, iv->getContentSize().height * 0.025f));
	itemImage2->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	iv->addChild(itemImage2);
	ui::ImageView* currencyImage = ui::ImageView::create("Shop/currency.png");
	currencyImage->setPosition(Vec2(-iv->getContentSize().width * 0.25f, iv->getContentSize().height * 0.025f));
	currencyImage->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	iv->addChild(currencyImage);

	ui::Text* text = ui::Text::create();
	text->setName("currencyText");
	text->setFontName("arial");
	text->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	text->setScale(5);
	text->setText(std::to_string(player->currency));
	text->setPosition(Vec2(-iv->getContentSize().width * 0.15f,  0.00f));
	iv->addChild(text);


	MenuItemImage* menuitem = MenuItemImage::create("Shop/purchase.png", "Shop/purchaseDown.png", CC_CALLBACK_1(HelloWorld::PurchaseDankuusen, this));
	menuitem->setPosition(/*(iv->getPosition() * 0.5) + */Vec2(0, iv->getContentSize().height * 0.075f));
	//menuitem->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	//menuitem->setPosition(Vec2(iv->getContentSize().width * 0.05f, iv->getContentSize().width * 0.5f));
	//itemImage->addChild(menuitem);

	MenuItemImage* menuitem2 = MenuItemImage::create("Shop/purchase.png", "Shop/purchaseDown.png", CC_CALLBACK_1(HelloWorld::PurchaseFlashjump, this));
	menuitem2->setPosition(/*(iv->getPosition() * 0.5) + */Vec2(0, iv->getContentSize().height * -0.4f));
	//menuitem2->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	//menuitem->setPosition(Vec2(iv->getContentSize().width * 0.05f, iv->getContentSize().width * 0.5f));
	//itemImage2->addChild(menuitem2);


	Menu* menu = Menu::create(menuitem, menuitem2, NULL);
	menu->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
	menu->setName("menu");
	//menu->setPosition(playingSize.width * 0.5f, playingSize.height * 0.5f);
	//menu->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	this->addChild(menu, 3);
	menu->setVisible(false);
	//menu->setVisible(true);

	this->addChild(menuNode, 2);
	menuNode->setVisible(false);

	auto menuButton = ui::Button::create("Shop/currency.png", "Shop/shopCross");
	menuButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::ToggleShop, this));
	menuButton->setPosition(playingSize * 0.95f);
	this->addChild(menuButton, 2);
	//menuNode->setVisible(true);
		//itemImage->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

		//Menu* menu = Menu::createWithItem(itemImage);
		//menu->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		//menu->setPosition(Vec2(playingSize.width * 0.55, (playingSize.height * 0.2f)));
		//menu->setName("menu");

		//Interact Button
		//auto menu = ui::Button::create("UpButtonUp.png", "UpButtonDown.png", "UpButtonDown.png");
		//itemImage->setPosition(Vec2(playingSize.width * 0.55, (playingSize.height * 0.5f)));
		//itemImage->addTouchEventListener(CC_CALLBACK_2(HelloWorld::InteractButtonResponse, this));

		//this->addChild(menu, 2);
		//	skillIcon->setPosition(Vec2::ANCHOR_MIDDLE);

		//Back Button
	auto backButton = ui::Button::create("LeftButtonUp.png", "LeftButtonDown.png", "LeftButtonDown.png");
	backButton->setPosition(Vec2(playingSize.width * 0.05, (playingSize.height * 1.f)));
	backButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::BackButtonResponse, this));
	backButton->setScale(.7f);

	//this->addChild(moveLeftButton, 1);
	//this->addChild(moveRightButton, 1);
	this->addChild(jumpButton, 1);
	this->addChild(interactButton, 1);
	this->addChild(skillButton, 1);
	this->addChild(backButton, 1);

	//-------------------------------Camera masks
	//Director::getInstance()->getRunningScene()->setCameraMask((unsigned short)CameraFlag::USER1);
	this->setCameraMask((unsigned short)CameraFlag::USER1);

	joystick->setCameraMask((unsigned short)CameraFlag::DEFAULT);

	//moveLeftButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	//moveRightButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	jumpButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	interactButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	skillButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	backButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	paraSprite->setCameraMask((unsigned short)CameraFlag::USER1);
	//text->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	menuNode->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	menu->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	menuButton->setCameraMask((unsigned short)CameraFlag::DEFAULT);
	//menu->setCameraMask((unsigned short)CameraFlag::DEFAULT);

	//log("camera mask: %i", Camera::getDefaultCamera()->getCameraMask());
	//log("scene mask: %i",this->getCameraMask());
	//log("left mask: %i", moveLeftButton->getCameraMask());
	//log("right mask: %i", moveRightButton->getCameraMask());

	Size winSize = Director::getInstance()->getVisibleSize();
	Vec2 defPos = Camera::getDefaultCamera()->getPosition();
	log("X: %f, Y: %f", defPos.x, defPos.y);
	Camera::getDefaultCamera()->setPosition(0, 0);
	defPos = Camera::getDefaultCamera()->getPosition();
	log("X: %f, Y: %f", defPos.x, defPos.y);
	return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}


void HelloWorld::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_UP_ARROW:
	{
		Joystick* stick = (Joystick*)(this->getChildByName("Joystick"));
		stick->SetJoystickPos(stick->currentDirection + Vec2(0, 1));
	}
	break;
	case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
	{
		Joystick* stick = (Joystick*)(this->getChildByName("Joystick"));
		stick->SetJoystickPos(stick->currentDirection + Vec2(0, -1));
	}
	break;
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
	{
		Joystick* stick = (Joystick*)(this->getChildByName("Joystick"));
		stick->SetJoystickPos(stick->currentDirection + Vec2(-1, 0));
	}
	break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
	{
		Joystick* stick = (Joystick*)(this->getChildByName("Joystick"));
		stick->SetJoystickPos(stick->currentDirection + Vec2(1, 0));
	}
	break;
	case EventKeyboard::KeyCode::KEY_SPACE:
	{
		b_upButtonHold = true;
		((Player*)(this->getChildByName("spriteNode")->getChildByName("player")))->FlashJump();
	}
	break;

	case EventKeyboard::KeyCode::KEY_TAB:
	{
		this->getChildByName("menu")->setVisible(!this->getChildByName("menu")->isVisible());
		break;
	}
	}
}

void HelloWorld::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_UP_ARROW:
	{
		Joystick* stick = (Joystick*)(this->getChildByName("Joystick"));
		stick->SetJoystickPos(stick->currentDirection - Vec2(0, 1));
	}
	break;
	case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
	{
		Joystick* stick = (Joystick*)(this->getChildByName("Joystick"));
		stick->SetJoystickPos(stick->currentDirection - Vec2(0, -1));
	}
	break;
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
	{
		Joystick* stick = (Joystick*)(this->getChildByName("Joystick"));
		stick->SetJoystickPos(stick->currentDirection - Vec2(-1, 0));
	}
	break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
	{
		Joystick* stick = (Joystick*)(this->getChildByName("Joystick"));
		stick->SetJoystickPos(stick->currentDirection - Vec2(1, 0));
	}
	break;
	case EventKeyboard::KeyCode::KEY_SPACE:
	{

		b_upButtonHold = false;
	}
	break;
	}
}

void HelloWorld::onMouseMove(Event* event)
{
	EventMouse* e = (EventMouse*)event;
}

void TempTest()
{
	Director::getInstance()->getRunningScene()->getChildByTag(999)->getChildByName("spriteNode")->getChildByName("mainSprite")->stopActionByTag(ActionTags::ANIMATION);
}
void HelloWorld::onMouseUp(Event* event)
{
	EventMouse* e = (EventMouse*)event;
	//e->getMouseButton();

	//auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	Vector<FiniteTimeAction*> arrayOfActions;

	//!!!TODO change this part to make it be physics based instead of move to 
	arrayOfActions.pushBack(MoveTo::create(2, mousePos));
	arrayOfActions.pushBack(CallFunc::create(
		[]()
		{
			auto scene = Director::getInstance()->getRunningScene();
			auto thisScene = scene->getChildByTag(999);
			thisScene->getChildByName("spriteNode")->getChildByName("player")->stopActionByTag(ANIMATION);
		}
	));
	auto moveEvent = Sequence::create(arrayOfActions);
	moveEvent->setTag(MOVEMENT);

	auto player = this->getChildByName("spriteNode")->getChildByName("player");
	//auto dir = mousePos - curSprite->getPosition();
	auto dir = mousePos - player->getPosition();
	//Vector<SpriteFrame*> animFrames;
	//animFrames.reserve(4);
	Animation* anim = nullptr;

	if (abs(dir.x) > abs(dir.y))
	{
		//Left Right
		if (dir.x > 0) //right need reverse scale due to main sprite facing left
		{
			//anim = AnimationCache::getInstance()->getAnimation("walk_right");
			anim = AnimationCache::getInstance()->getAnimation("mushmummy_idle");
			//curSprite->setScaleX(-abs(curSprite->getScaleX()));
			player->setScaleX(-abs(player->getScaleX()));
		}
		else
		{
			anim = AnimationCache::getInstance()->getAnimation("mushmummy_idle");
			//curSprite->setScaleX(abs(curSprite->getScaleX()));
			player->setScaleX(abs(player->getScaleX()));
		}

	}
	else
	{
		anim = AnimationCache::getInstance()->getAnimation("mushmummy_attack");
	}
	//curSprite->stopActionByTag(MOVEMENT);
	//curSprite->runAction(moveEvent);

	//curSprite->stopActionByTag(ANIMATION);
	//auto moveAnim = RepeatForever::create(Animate::create(anim));
	//moveAnim->setTag(ANIMATION);
	//curSprite->runAction(moveAnim);

	player->stopActionByTag(MOVEMENT);
	player->runAction(moveEvent);

	player->stopActionByTag(ANIMATION);
	auto moveAnim = RepeatForever::create(Animate::create(anim));
	moveAnim->setTag(ANIMATION);
	player->runAction(moveAnim);
}

void HelloWorld::onMouseDown(Event* event)
{
	EventMouse* e = (EventMouse*)event;

	mousePos.x = e->getCursorX();
	mousePos.y = e->getCursorY();
}

void HelloWorld::onMouseScroll(cocos2d::Event* event)
{
}

#pragma region Button Responses
void HelloWorld::LeftButtonResponse(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
	auto player = (Player*)(this->getChildByName("spriteNode")->getChildByName("player"));
	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		player->MoveLeft();
		b_leftButtonHold = true;
		break;
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
		player->Idle();
		b_leftButtonHold = false;
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		b_leftButtonHold = false;
		player->Idle();
		break;
	default:
		break;
	}
}
void HelloWorld::RightButtonResponse(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
	auto player = (Player*)(this->getChildByName("spriteNode")->getChildByName("player"));
	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		player->MoveRight();
		b_rightButtonHold = true;
		//PlayerMoveRight();
		break;
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		//PlayerMoveRight();
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
		player->Idle();
		b_rightButtonHold = false;
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		b_rightButtonHold = false;
		player->Idle();
		break;
	default:
		break;
	}
}
void HelloWorld::JumpButtonResponse(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
	{
		b_upButtonHold = true;

		Player* player = (Player*)(this->getChildByName("spriteNode")->getChildByName("player"));
		//PlayerJump();
		player->FlashJump();
	}
	break;
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
		b_upButtonHold = false;
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		b_upButtonHold = false;
		break;
	default:
		break;
	}
}

void HelloWorld::InteractButtonResponse(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
	if (onPortal)
	{
		//one day we will save the scene
		Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.5f, HelloWorld2::createScene()));
	}
}

void HelloWorld::BackButtonResponse(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
	Director::getInstance()->replaceScene(EmptyScene::createScene());
}

bool HelloWorld::onContactBegin(PhysicsContact& begin)
{
	auto bodyA = begin.getShapeA()->getBody();
	auto bodyB = begin.getShapeB()->getBody();
	//Check if is lower layer
	if (bodyB->getCategoryBitmask() < bodyA->getCategoryBitmask())
	{
		//Swap
		auto temp = bodyB;
		bodyB = bodyA;
		bodyA = temp;
	}

	//Player collide with a trigger
	if (bodyA->getCategoryBitmask() == LayerPlayer && bodyB->getCategoryBitmask() == LayerTrigger)
	{
		if (bodyB->getOwner()->getName() == "portal")
		{
			onPortal = true;
		}
		else if (bodyB->getOwner()->getName() == "rope")
		{
			Player* player = (Player*)(bodyA->getOwner());
			player->touchingRope = true;
			player->ropeXPos = bodyB->getPosition().x;

			log("enter rope, x: %f", player->ropeXPos);
		}
	}

	//Player collide with ground
	if (bodyA->getCategoryBitmask() == LayerPlayer && bodyB->getCategoryBitmask() == LayerGround)
	{
		if (bodyA->getOwner()->getName() == "player")
		{
			((Player*)(bodyA->getOwner()))->isGrounded = true;
			((Player*)(bodyA->getOwner()))->flashJump = true;
		}
	}
	//Player collide with enemy
	if (bodyA->getCategoryBitmask() == LayerPlayer && bodyB->getCategoryBitmask() == LayerEnemy)
	{
		Entity* owner = (Entity*)(bodyB->getOwner());
		//making animation
		//curSprite->setScaleX(abs(curSprite->getScaleX())); //aligning sprite to correct direction (default- left)

		owner->Die();

		//Reduce the count
		enemySpawner->EnemyDied(1);
		bodyB->setEnabled(false);

		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Audio/Arrow.wav");
	}
	//log(bodyA->getOwner()->getName().c_str());
	log("body 1: %d, body 2: %d", bodyA->getCategoryBitmask(), bodyB->getCategoryBitmask());

	return true;
}

//On Contact End
bool HelloWorld::onContactSeperate(cocos2d::PhysicsContact& begin)
{
	auto bodyA = begin.getShapeA()->getBody();
	auto bodyB = begin.getShapeB()->getBody();
	//Check if is lower layer
	if (bodyB->getCategoryBitmask() < bodyA->getCategoryBitmask())
	{
		//Swap
		auto temp = bodyB;
		bodyB = bodyA;
		bodyA = temp;
	}
	//Player seperate from ground
	if (bodyA->getCategoryBitmask() == LayerPlayer && bodyB->getCategoryBitmask() == LayerGround)
	{
		//Player* player = (Player*)(bodyA->getOwner());
	}

	//Player seperate with a trigger
	if (bodyA->getCategoryBitmask() == LayerPlayer && bodyB->getCategoryBitmask() == LayerTrigger)
	{
		Player* player = (Player*)(bodyA->getOwner());
		if (bodyB->getOwner()->getName() == "portal")
		{
			onPortal = false;
		}
		else if (bodyB->getOwner()->getName() == "rope")
		{
			player->touchingRope = false;
			player->UnhangRope();
			log("leave rope");
		}
	}

	return true;
}

void HelloWorld::PurchaseDankuusen(cocos2d::Ref* sender)
{
	auto btn = this->getChildByName("Skill Button");
	auto comp = (SkillComponent*)(btn->getComponent("Skill Component"));

	comp->SetSkill(SkillComponent::Skills::NothingPersonalKid);

	return;
}

void HelloWorld::PurchaseFlashjump(cocos2d::Ref* sender)
{
	Player* player = (Player*)(this->getChildByName("spriteNode")->getChildByName("player"));

	player->flashJumpUnlocked = true;

	return;
}

void HelloWorld::ToggleShop(cocos2d::Ref* sender)
{
	Menu* menu = (Menu*)this->getChildByName("menu");
	Node* menuNode = this->getChildByName("menuNode");

	menu->setVisible(!menu->isVisible());
	menuNode->setVisible(!menuNode->isVisible());
}

#pragma endregion


void HelloWorld::update(float dt)
{
	SceneBase::update(dt);

	//auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");

	Sprite* bgSprite = (Sprite*)(this->getChildByName("Background"));
	//Vec2 targetCamPos = curSprite->getPosition();

	Player* player = (Player*)(this->getChildByName("spriteNode")->getChildByName("player"));
	StateMachine* stateMachine = (StateMachine*)(this->getChildByName("stateMachine"));
	for (auto enemy : this->getChildByName("enemyNode")->getChildren())
	{
		((Enemy*)enemy)->stateTimer -= dt;
		if (((Enemy*)enemy)->stateTimer < 0.0f)
		{
			((Enemy*)enemy)->stateTimer = rand() % 4 + 2;
			stateMachine->Update(dt, (Enemy*)enemy);
			//StateMachine::getInstance()->Update(dt, (Enemy*)enemy);
		}
	}
	((cocos2d::ui::Text*) this->getChildByName("menuNode")->getChildByName("shopScreen")->getChildByName("currencyText"))->setText(std::to_string(player->currency));

	//sm->Update(dt,)

//	log(  "%f",player->getPosition().x);

	Vec2 targetCamPos = player->getPosition();

	Vec2 camBounds = Vec2(
		bgSprite->getContentSize().width * bgSprite->getScaleX() - Director::getInstance()->getVisibleSize().width * .5f,
		bgSprite->getContentSize().height * bgSprite->getScaleY() + Director::getInstance()->getVisibleSize().height * .5f
	);
	//Check X
	targetCamPos.x = clamp(targetCamPos.x, camBounds.x * 0.5, bgSprite->getPositionX() + camBounds.x);
	//Check Y
	targetCamPos.y = clamp(targetCamPos.y, camBounds.y * 0.25, bgSprite->getPositionY() + camBounds.y);

	worldCamera->setPosition(targetCamPos);

	//Joystick input
	auto joystick = (Joystick*)(this->getChildByName("Joystick"));
	if (joystick->active && !player->skillLock)
	{
		//	log("Joystick is active");
			//Dont save as variable in joystick, muh memory
		Vec2 dir = joystick->currentDirection;
		//Get direction of the joystick
		float dot = dir.dot(Vec2(1, 0)) / dir.getLength();
		//get direction
		//If left/right majority
		if (std::abs(dot) > .77f)
		{
			//Right
			if (dir.x > 0.f)
			{
				player->MoveRight();
			}
			//Left
			else
			{
				player->MoveLeft();
			}
		}
		//Up/down majority
		else
		{
			//Up
			if (dir.y > 0.f)
			{
				//Hang on rope/go up rope
				player->MoveUp();
			}
			//Down
			else
			{
				//Crouch?/go down rope
				player->MoveDown();
			}
		}
	}

	//Jump is a button and shld stay a button
	//Check skill anim lock ig hardcode but wtver
	if (b_upButtonHold && !player->skillLock)
	{
		if (!player->onRope)
			player->Jump();
		else
		{
			if (joystick->active)
			{			//Only jump when a direction is selected
				Vec2 dir = joystick->currentDirection;
				//Get direction of the joystick
				float dot = dir.dot(Vec2(1, 0));
				if (std::abs(dot) > .77f)
					player->Jump();
			}
			else if (joystick == NULL)
				player->Jump();
		}
	}
}


/*
void HelloWorld::onMouseScroll(Event * event)
{
	EventMouse* e = (EventMouse*)event;

	auto curSprite = this->getChildByName("spriteNode")->getChildByName("mainSprite");
	auto moveEvent = MoveBy::create(0.f, Vec2(0, -e->getScrollY()));
	curSprite->runAction(moveEvent);
}
*/