/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "SceneBase.h"
#include "Entity/StateMachine.h"
#include "Entity/StateLeader.h"
 //enum LayerEnums
 //{
 //	LayerTrigger	= 0x00,
 //	LayerPlayer		= 0x01,
 //	LayerGround		= 0x02,
 //	LayerEnemy		= 0x04,
 //	Layer4			= 0x08,
 //	Layer5			= 0x16,
 //	Layer6			= 0x32,
 //	Layer7			= 0x64,
 //	Layer8			= 0x128,
 //	Layer9			= 0x256,
 //};

class HelloWorld : public SceneBase
{
	//cocos2d::Vec2 mousePos;
	//cocos2d::Camera* worldCamera;
		
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);

	// implement the "static create()" method manually
	CREATE_FUNC(HelloWorld);


	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void onMouseMove(cocos2d::Event* event);
	void onMouseUp(cocos2d::Event* event);
	void onMouseDown(cocos2d::Event* event);
	void onMouseScroll(cocos2d::Event* event);

	//Button responses
	void LeftButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type);
	void RightButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type);
	void JumpButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type);
	void InteractButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type);
	void BackButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type);

	bool onContactBegin(cocos2d::PhysicsContact& begin);
	bool onContactSeperate(cocos2d::PhysicsContact& begin);

	void PurchaseDankuusen(cocos2d::Ref* sender);
	void PurchaseFlashjump(cocos2d::Ref* sender);
	void ToggleShop(cocos2d::Ref* sender);
	//void PlayerMoveLeft();
	//void PlayerMoveRight();
	//void PlayerJump();
	//void PlayerStop();

	void update(float dt);
	//void SpawnEnemies();
	//bool isGrounded;//temp

	//bool b_rightButtonHold = false;
	//bool b_leftButtonHold = false;
	//bool b_upButtonHold = false;

	//Pls change this
	//bool onPortal = false;


	//cocos2d::CCTMXTiledMap* tilemap;
	//cocos2d::Vector<cocos2d::SpriteFrame*> sprites;

	//const float respawnTimerReset = 3.0f;
	//float respawnTimer  = respawnTimerReset;

	//StateMachine * sm;
};
#endif // __HELLOWORLD_SCENE_H__
