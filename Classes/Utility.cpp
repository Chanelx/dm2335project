#include "Utility.h"

int Utility::wrap(int i, int min, int max)
{
	return (i >= max) ? wrap(min + i - max, min, max) : (i < min ? (wrap(max + i - min, min, max)) : i);
}

float Utility::clamp(float i, float min, float max)
{
	return i >= max ? max : i <= min ? min : i;
}
