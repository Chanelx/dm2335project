/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "EmptyScene.h"
#include "SimpleAudioEngine.h"
#include "HelloWorldScene.h"
#include "HelloWorldScene2.h"

USING_NS_CC;
//kermit suicide
Scene* EmptyScene::createScene()
{
	auto scene = Scene::create();
	//auto scene = Scene::createWithPhysics();
	auto layer = EmptyScene::create();
	scene->addChild(layer, 0, 999);
	//scene->getPhysicsWorld()->setDebugDrawMask(0xffff);
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in EmptySceneScene.cpp\n");
}

// on "init" you need to initialize your instance
bool EmptyScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	Size playingSize = Size(visibleSize.width, visibleSize.height - (visibleSize.height / 8));

	//Node Container
	//auto nodeItems = Node::create();
	//nodeItems->setName("nodeItems");

	////Ground
	//auto sprite = Sprite::create("ZigzagGrass_Mud_Round.png");
	//auto currTexture = sprite->getTexture();
	////Create the first
	//sprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	//sprite->setPosition(0, 0);

	//adding physics body
	//auto physicsBody = PhysicsBody::createBox(Size(sprite->getContentSize().width, sprite->getContentSize().height), PhysicsMaterial(0.1f, 0.5f, 0.0f));
	//physicsBody->setDynamic(false);
	//sprite->addComponent(physicsBody);

	//nodeItems->addChild(sprite, 0);

	//float currentWidth = currTexture->getPixelsWide();
	//while (currentWidth < playingSize.width)
	//{
	//	sprite = Sprite::create("ZigzagGrass_Mud_Round.png");

	//	sprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	//	sprite->setPosition(currentWidth, 0);

	//	//adding physics body
	//	//physicsBody = PhysicsBody::createBox(Size(sprite->getContentSize().width, sprite->getContentSize().height), PhysicsMaterial(0.1f, 0.5f, 0.0f));
	//	//physicsBody->setDynamic(false);
	//	//sprite->addComponent(physicsBody);

	//	nodeItems->addChild(sprite, 0);

	//	currentWidth += currTexture->getPixelsWide();
	//}

	//nodeItems->setPositionY(playingSize.height * 0.5f);
	//this->addChild(nodeItems, 1);

	/*
	//Sprites
	auto spriteNode = Node::create();
	spriteNode->setName("spriteNode");

	auto mainSprite = Sprite::create("Blue_Front1.png");
	mainSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	mainSprite->setPosition(100, playingSize.height * 0.75f);
	mainSprite->setName("mainSprite");

	////adding physics body
	//physicsBody = PhysicsBody::createBox(Size(mainSprite->getContentSize().width, mainSprite->getContentSize().height), PhysicsMaterial(0.1f, 0.5f, 0.0f));
	////physicsBody->setDynamic(true);	
	//mainSprite->addComponent(physicsBody);

	spriteNode->addChild(mainSprite, 1);

	
	//Sprite Sheet
	SpriteBatchNode* spriteBatch = SpriteBatchNode::create("sprite.png");
	SpriteFrameCache* cache = SpriteFrameCache::getInstance();
	cache->addSpriteFramesWithFile("sprite.plist");
	AnimationCache::getInstance()->addAnimationsWithFile("sprite_ani.plist");

	//making mushmummy test
	SpriteBatchNode* mushBatch = SpriteBatchNode::create("mushmummySprite.png");
	cache->addSpriteFramesWithFile("mushmummy.plist");
	AnimationCache::getInstance()->addAnimationsWithFile("mushmummy_ani.plist");


	ButtonManager* buttonManager = new ButtonManager(this, playingSize);
	//buttonManager->InitButtonManager();
	*/

	//this->addChild(spriteNode, 1);
	auto startButton = ui::Button::create("RightButtonUp.png", "RightButtonDown.png", "RightButtonDown.png");
	startButton->setPosition(Vec2(playingSize.width * 0.5f, playingSize.height * 0.5f));
	startButton->addTouchEventListener(CC_CALLBACK_2(EmptyScene::ButtonResponse, this));
	startButton->setScale(5);
	this->addChild(startButton, 10);



	//Events
	
	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(EmptyScene::onKeyPressed, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	
	//auto mouseListener = EventListenerMouse::create();
	//mouseListener->onMouseMove = CC_CALLBACK_1(EmptyScene::onMouseMove, this);

	//mouseListener->onMouseUp = CC_CALLBACK_1(EmptyScene::onMouseUp, this);

	//mouseListener->onMouseDown = CC_CALLBACK_1(EmptyScene::onMouseDown, this);

	//mouseListener->onMouseScroll = CC_CALLBACK_1(EmptyScene::onMouseScroll, this);
	//_eventDispatcher->addEventListenerWithSceneGraphPriority(mouseListener, this);

	////Touch Event
	//auto touchListener = EventListenerTouchOneByOne::create();
	//touchListener->onTouchBegan = [](Touch* touch, Event* event)
	//{
	//	return true;
	//};
	//touchListener->onTouchMoved = [](Touch* touch, Event* event)
	//{
	//	return true;
	//};
	//touchListener->onTouchEnded = [](Touch* touch, Event* event)
	//{
	//	return true;
	//};
	//_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	////Animation
	//sprites.reserve(16);
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_idle1.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_idle2.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_idle3.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_idle4.png"));

	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_idle5.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_attack1.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_attack2.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_attack3.png"));

	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_hurt.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_dead1.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_dead2.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_dead3.png"));

	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_dead4.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_dead5.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("mushmummy_dead6.png"));
	//sprites.pushBack(cache->getSpriteFrameByName("Blue_Right1.png"));

	return true;
}


void EmptyScene::menuCloseCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);
}

void EmptyScene::onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event * event)
{
	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_SPACE:
		Director::getInstance()->pushScene(HelloWorld::createScene());

		break; 
	case EventKeyboard::KeyCode::KEY_BACKSPACE:
		Director::getInstance()->popScene();
		break;
	}
}

void EmptyScene::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event * event)
{
}

void EmptyScene::onMouseMove(cocos2d::Event * event)
{
}

void EmptyScene::onMouseUp(cocos2d::Event * event)
{
}

void EmptyScene::onMouseDown(cocos2d::Event * event)
{
}

void EmptyScene::onMouseScroll(cocos2d::Event * event)
{
}

void EmptyScene::LeftButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type)
{
}

void EmptyScene::RightButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type)
{
}

void EmptyScene::JumpButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type)
{
}

void EmptyScene::InteractButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type)
{
}

void EmptyScene::BackButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type)
{
}

bool EmptyScene::onContactBegin(cocos2d::PhysicsContact & begin)
{
	return false;
}

bool EmptyScene::onContactSeperate(cocos2d::PhysicsContact & begin)
{
	return false;
}

void EmptyScene::ButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type)
{
	if(type == cocos2d::ui::Widget::TouchEventType::BEGAN)
		Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.5f, HelloWorld::createScene()));

}
