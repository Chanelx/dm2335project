#ifndef _SCENEBASE_H__
#define _SCENEBASE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "Utility.h"
#include "SimpleAudioEngine.h"
#include "Entity/Player.h"
#include "Entity/EnemySpawner.h"
#include "Joystick/Joystick.h"

//Game Scene, ie Scenes that have actual gameplay, not just main menu/start/wtver
class SceneBase : public cocos2d::Scene
{
protected:
	cocos2d::Vec2 mousePos;
	cocos2d::Camera* worldCamera;

	//Spawns enemies automatically if limit not reached
	EnemySpawner* enemySpawner;
public:
	//SceneBase();
	//~SceneBase();
	virtual bool init();
	virtual void update(float dt);

	//cocos2d::Scene* createScene();
	// a selector callback
	virtual void menuCloseCallback(cocos2d::Ref* pSender) = 0;

	// implement the "static create()" method manually
	//CREATE_FUNC(SceneBase);

	virtual	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) = 0;
	virtual	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) = 0;
	virtual	void onMouseMove(cocos2d::Event* event) = 0;
	virtual	void onMouseUp(cocos2d::Event* event) = 0;
	virtual	void onMouseDown(cocos2d::Event* event) = 0;
	virtual	void onMouseScroll(cocos2d::Event* event) = 0;

	//Button responses
	virtual	void LeftButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type) = 0;
	virtual	void RightButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type) = 0;
	virtual	void JumpButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type) = 0;
	virtual	void InteractButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type) = 0;
	virtual	void BackButtonResponse(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType type) = 0;

	virtual	bool onContactBegin(cocos2d::PhysicsContact& begin) = 0;
	virtual	bool onContactSeperate(cocos2d::PhysicsContact& begin) = 0;

	//make a player class with these
	//void PlayerMoveLeft();
	//void PlayerMoveRight();
	//void PlayerJump();
	//void PlayerStop();
	//void SpawnEnemies();
	bool isGrounded;//temp
	//Pls change this
	bool onPortal = false;

	virtual	cocos2d::TMXTiledMap* InitializeTilemap(std::string filePath, std::string name, cocos2d::Vec2 position, float scale);
	virtual void SpawnEnemies();


	bool b_rightButtonHold = false;
	bool b_leftButtonHold = false;
	bool b_upButtonHold = false;

	//cocos2d::Vector<cocos2d::SpriteFrame*> sprites;

	const float respawnTimerReset = 3.0f;
	float respawnTimer = respawnTimerReset;

	virtual bool InitializeObjects();

	CocosDenshion::SimpleAudioEngine* audioSource;


	static bool pointInPolygon(cocos2d::Vec2 point, std::vector<cocos2d::Vec2> polygon, int n);

	//Test
	cocos2d::Vector<cocos2d::Sprite*> entities;
	~SceneBase();
};
#endif // _SCENEBASE_H_


